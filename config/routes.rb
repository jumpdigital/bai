Rails.application.routes.draw do

  #devise_for :users
  devise_for :users
  devise_for :customers, :controllers => { :registrations => 'customer/registrations', :passwords => 'customer/passwords' }

  #root 'application#angular'
  root 'home#index'
  namespace :api, :defaults => { :format => :json } do

    devise_scope :customers do
      post '/customers/signup' => 'customers#store', :as => 'registration'
      post '/customers/login' => 'customers#login', :as => 'login'
      delete '/customers/logout' => 'customers#logout', :as => 'logout'
      patch '/customers/update' => 'customers#update', :as => 'update_user'
      get '/customers/show' => 'customers#show', :as => 'vew_user'
      patch '/customers/password' => 'customers#change_password', :as           => 'change_password'
      post '/customers/recover' => 'customers#generate_new_password_email', :as => 'recover_password'
    end

    #catalog
    get '/products/all' => 'products#index', :as => 'customer_all_products'
    get '/categories/all' => 'categories#index', :as => 'customer_all_categories'
    get '/subcategories/all' => 'sub_categories#index', :as => 'customer_all_subcategories'
      # root to: 'main#index'

      # get 'dashboard', to: 'main#dashboard'

      # # get roles json
      # get 'users/roles', to: 'users#roles'

      # devise_scope :users do
      #   # post 'users' => 'users#create', :as => 'create_user'
      #   get 'user' => 'users#profile', :as => 'profile'
      #   patch 'user' => 'users#update_profile', :as => 'update_profile'
      #   # get 'users' => 'users#index', :as => 'all_users'


      #   post 'login' => 'users#login', :as => 'login'
      #   delete 'logout' => 'users#logout', :as => 'logout'
      #   patch '/password' => 'users#change_password', :as => 'change_password'

      #   post 'forgot-password' => 'users#forgot_password', :as => 'forgot_password'
      #   get 'reset-password.:id' => 'users#reset_password', :as => 'edit_password'
      #   put 'reset-password' => 'users#update_password', :as => 'update_password'
      # end

      # resources :users
      # resources :categories
      # resources :products
      # resources :customers

      # devise_for :users

    #admin api

    # root to: 'main#index'

    get 'dashboard', to: 'main#dashboard'

    # get roles json
    get 'users/roles', to: 'users#roles'

    devise_scope :users do
      # post 'users' => 'users#create', :as => 'create_user'
      get 'users/current' => 'admin/users#profile', :as => 'profile'
      patch 'users' => 'admin/users#update_profile', :as => 'update_profile'
      #get 'users' => 'admin/users#index', :as => 'all_users'


      post 'users/login' => 'admin/users#login', :as => 'admin_login'
      delete 'users/logout' => 'admin/users#logout', :as => 'admin_logout'
      post 'users/password' => 'admin/users#change_password', :as => 'admin_change_password'

      patch 'users/password' => 'admin/users#forgot_password', :as => 'admin_forgot_password'
      get 'users/password.:id' => 'admin/users#reset_password', :as => 'admin_edit_password'
      put 'users/password' => 'admin/users#update_password', :as => 'admin_update_password'
    end

      resources :users, module: "admin" do
        collection { post :import
                     get :export
                    }
      end
      resources :categories, module: "admin"
      resources :products, module: "admin" do
        collection { post :import
                     get :export
                    }
      end
      resources :customers, module: "admin" do
        collection { post :import
                     get :export
                    }
      end
      resources :orders, module: "admin" do
        collection { get :export
                    get :graph
                    get :last_five_orders
                    }
      end
      resources :order_statuses, module: "admin"
      resources :order_histories, module: "admin"
      resources :roles, module: "admin"
      resources :product_galleries, module: "admin"
      resources :promos, module: "admin"
      resources :sub_categories, module: "admin"
      resources :promo_types, module: "admin"
      resources :amount_types, module: "admin"
      resources :general_settings, module: "admin"
      resources :banners, module: "admin"
      resources :shipping_zone, module: "admin"
      resources :shipping, module: "admin" 
      resources :tax_setting, module: "admin"

      # get 'settings/general/:id' => 'admin/general_setting#show', :as => 'show_general_setting'

      #multiple delete
      delete 'customers' => 'admin/customers#destroy', :as => 'delete_customers'
      post 'products/delete' => 'admin/products#multiple_delete', :as => 'multiple_delete_products'
      post 'categories/delete' => 'admin/categories#multiple_delete', :as => 'multiple_delete_categories'
      post 'galleries/delete' => 'admin/product_galleries#multiple_delete', :as => 'multiple_delete_galleries'
      post 'sub_categories/delete' => 'admin/sub_categories#multiple_delete', :as => 'multiple_delete_subcategories'
      post 'promos/delete' => 'admin/promos#multiple_delete', :as => 'multiple_delete_promos'
      post 'banners/delete' => 'admin/banners#multiple_delete', :as => 'multiple_delete_banners'

      post 'orders/status/' => 'admin/orders#change_status', :as => 'change_status'

      #filter
      post 'orders/filter/' => 'admin/orders#filter', :as => 'filter_order'


      #deactivate account
      put 'customers/deactivate/:id' => 'admin/customers#deactivate_activate', :as => 'customer_deactivate'
      put 'users/deactivate/:id' => 'admin/users#deactivate_activate', :as => 'user_deactivate'

      #for inventory

      get 'all_products' => 'admin/products#all_products', :as => 'all_products'
      get 'all_orders' => 'admin/orders#all_orders', :as => 'all_orders'

      get 'dynamic_values' => 'admin/orders#dynamic_values', :as => 'dynamic_values'
      #validate promo
      post 'orders/promo' => 'admin/orders#validate_promo', :as => 'validate_promo'

      #cancel order
      post 'orders/cancel' => 'admin/orders#cancel_order', :as => 'cancel_order'

      #tags
      get 'tags' => 'admin/products#tags', :as => 'tags'


    # devise_for :users

    get 'cart', to: 'cart#index'
    post 'cart', to: 'cart#add'
    patch 'cart', to: 'cart#update'
    delete 'cart', to: 'cart#remove'
    delete 'cart/clear', to: 'cart#clear'

  end

  get '/productlist' => 'products#index', :as => 'products', :defaults => { :format => 'json' }
  post '/product' => 'products#search', :as => 'product', :defaults => { :format => 'json' }
  get '/product/:id' => 'products#show', :as => 'show_product', :defaults => { :format => 'json' }

  post '/order' => 'orders#add_to_cart', :as => 'add_to_cart', :defaults => { :format => 'json' }
  get '/cart' => 'orders#cart', :as => 'cart', :defaults => { :format => 'json' }

  namespace :admin do

    root to: 'main#index'

    get 'dashboard', to: 'main#dashboard'

    # get roles json
    get 'users/roles', to: 'users#roles'

    resources :users

    resources :categories
    resources :products
    resources :customers

  end
  #get '*path', to: 'application#angular'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
