class ProductsController < ApplicationController
  layout false
  respond_to :json
  def index
    respond_to do |format|
      format.json {
        render json: Product.all.to_json(:include => [:category => {}]), status: 200
      }
    end
  end

  def search
  	@product = Product.find_by_name(params[:name])
  	render json: @product.to_json(:include => [:category => {}]), status: 200

  end

  def show
    @product = Product.find(params[:id])
    render json: @product.to_json(:include => [:category => {}]), status: 200
  end
end