class OrdersController < ApplicationController
	layout false
  respond_to :json

  def add_to_cart
    if current_customer
      order = current_customer.order
      if order.count > 0
        case order.last.order_history.last.order_status_id
        when 1
          OrderProduct.save_order_product(order.last.id, params[:product_id], params[:quantity])
        when 2..4
          Order.save_to_cart(current_customer.id,params[:product_id], params[:quantity])
        end
      else
        Order.save_to_cart(current_customer.id,params[:product_id], params[:quantity])
      end
    else
      if cookies[:guest].present?
        user = cookies[:guest]
        cookies[:cart] = [
          "product_id" => params[:product_id],
          "quantity" => params[:quantity]

        ]
        # OrderProduct.save_order_product(order.last.id, params[:product_id], params[:quantity])
      else
        cookies[:guest] = SecureRandom.random_number(1_000_000)
      end
    end

    
    render json: order, status: 200
  	


  end

  def cart
    order = current_customer.order
    if order.count > 0
       case order.last.order_history.last.order_status_id
        when 1
          render json: order.to_json(:include => [
                                    :order_product => { :include => {:product => {}}}]), status: 200
        end      
    end
  end


  
end
