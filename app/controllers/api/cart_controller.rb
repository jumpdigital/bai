class Api::CartController < ApplicationController

  include Api::ReturnHelper

  layout false

  before_action :authenticate_token

  before_action :get_cart

  before_action :errors,
    only: [
      :index,
      :add,
      :update,
      :remove
    ]

  # list cart products
  def index
    render :json => { :cart => @cart, :products => @cart.order_products }, status: 200
  end

  # add products to cart
  def add
    # get product
    product = Product.find(params[:product_id])

    rescue ActiveRecord::RecordNotFound
      return render :json => { errors: ['Invalid product'] }, :status => 422
    else

      if params[:quantity].blank?
        @errors.push('quantity must not be blank')
      end
      if params[:quantity].present? && !is_number(params[:quantity].to_s)
        @errors.push('quantity must be a number')
      end

      if @errors.count > 0
        return render :json => { errors: @errors }, :status => 422
      end

      # check if already in cart
      cart_product = @cart.order_products.where(product_id: params[:product_id]).first
      if cart_product != nil
        cart_product.quantity = cart_product.quantity + params[:quantity].to_i
        cart_product.total_price = cart_product.total_price + (product.price * params[:quantity].to_i)

        if cart_product.save
          # update cart
          grand_total = 0
          @cart.order_products.each do |order_product|
            grand_total = grand_total + order_product.total_price
          end

          @cart.grand_total = grand_total

          if @cart.save
            render :json => { :cart => @cart, :products => @cart.order_products }, status: 200
          else
            render :json => { :errors => @cart.errors.full_messages }, :status => 422
          end
        else
          render :json => { :errors => cart_product.errors.full_messages }, :status => 422
        end
      else
        # add when not yet added
        cart_product = OrderProduct.new({
            order_id: @cart.id,
            product_id: product.id,
            quantity: params[:quantity],
            total_price: product.price * params[:quantity].to_i
          })
        if cart_product.save
          render :json => @cart, :status => 200
        else
          render :json => { :errors => cart_product.errors.full_messages }, :status => 422
        end
      end
  end

  # update product quantity
  def update
    # get product
    product = Product.find(params[:product_id])

    rescue ActiveRecord::RecordNotFound
      return render :json => { errors: ['Invalid product'] }, :status => 422
    else

      if params[:quantity].blank?
        @errors.push('quantity must not be blank')
      end
      if params[:quantity].present? && !is_number(params[:quantity].to_s)
        @errors.push('quantity must be a number')
      end

      if @errors.count > 0
        return render :json => { errors: @errors }, :status => 422
      end

      # check if in cart
      cart_product = @cart.order_products.where(product_id: params[:product_id]).first

      if cart_product != nil
        cart_product.quantity = params[:quantity].to_i
        cart_product.total_price = product.price * params[:quantity].to_i

        if cart_product.save
          # update cart
          grand_total = 0
          @cart.order_products.each do |order_product|
            grand_total = grand_total + order_product.total_price
          end

          @cart.grand_total = grand_total

          if @cart.save
            render :json => { :cart => @cart, :products => @cart.order_products }, status: 200
          else
            render :json => { :errors => @cart.errors.full_messages }, :status => 422
          end
        else
          render :json => { :errors => cart_product.errors.full_messages }, :status => 422
        end
      else
        # add when not yet added
        cart_product = OrderProduct.new({
            order_id: @cart.id,
            product_id: product.id,
            quantity: params[:quantity],
            total_price: product.price * params[:quantity].to_i
          })
          if cart_product.save
            # update cart
            grand_total = 0
            @cart.order_products.each do |order_product|
              grand_total = grand_total + order_product.total_price
            end

            @cart.grand_total = grand_total

            if @cart.save
              render :json => { :cart => @cart, :products => @cart.order_products }, status: 200
            else
              render :json => { :errors => @cart.errors.full_messages }, :status => 422
            end
          else
            render :json => { :errors => cart_product.errors.full_messages }, :status => 422
          end
      end
  end

  # remove products
  def remove
    # get product
    product = Product.find(params[:product_id])

    rescue ActiveRecord::RecordNotFound
      return render :json => { errors: ['Invalid product'] }, :status => 422
    else

      cart_product = @cart.order_products.where(product_id: product.id).first

      if cart_product.present?

        if cart_product.destroy
          render :json => @cart, :status => 200
        else
          return render :json => { errors: ['Could not remove product from cart. Please retry later.'] }, :status => 500
        end

      else
        return render :json => { errors: ['Invalid product.'] }, :status => 422
      end

  end

  # remove all products
  def clear
    # render :json => { cart: @cart }
  end

  private

  def get_cart
    if @customer != nil
      @cart = @customer.getCartOrSet
    else
      render :json => { error: "Invalid token" }, status: 403
    end
  end

  # check if string is a numeric
	def is_number string
  		true if Float(string) rescue false
	end

  def errors
    @errors = []
  end

end
