class Api::ProductsController < ApplicationController

	include Api::ReturnHelper

	# skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }
	respond_to :json

	#before_filter :authenticate_token, :only => [:logout, :update, :show, :change_password]

	def initialize
    @http_status = 400
    @http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
  end

	def index
	  success(Product.all)
		render :json => @http_response.to_json(:include => [
	    {:product_categories => { :only => [:category_id]}},
	    {:product_subcategories => { :only => [:sub_category_id]}},
	    {:product_gallery => {}}]),
	    :status => @http_status

	end

	protected

	def product_params
		 params.require(:product).permit(
      :category_id,
      :sku,
      :name,
      :description,
      :price,
      :image
      )
	end

end
