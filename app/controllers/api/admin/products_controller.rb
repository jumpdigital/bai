class Api::Admin::ProductsController < ApplicationController
  include Api::ReturnHelper
  layout false

  before_action :pass_model,
    :only => [
      :index,
      :create,
      :show,
      :update,
      :destroy
    ]

  before_action :get_product,
    only: [
      :show,
      :edit,
      :update,
      :destroy
    ]

  before_action :get_categories,
    only: [
      :new,
      :create,
      :edit,
      :update,
      :categories
    ]

  def initialize
    @http_status = 400
    @http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
  end

  def pass_model
    authenticate_token(User)
  end

  def all_products
    respond_to do |format|
      format.html {

      }
      format.json {
        success(Product.all)
        render :json => @http_response.to_json(:include => [
          {:product_tag => {}},
          {:product_categories => { :include => { :category => {:include => {:sub_categories => {}}}}}},
          {:product_subcategories => { :include => { :sub_category => {}}}},
          {:product_gallery => {}}]),
          :status => @http_status
      }
    end
  end

  def index
    respond_to do |format|
      format.html {

      }
      format.json {
        success(Product.all)
        render :json => @http_response.to_json(:include => [
          {:product_tag => {}},
          {:product_categories => { :include => { :category => {:include => {:sub_categories => {}}}}}},
          {:product_subcategories => { :include => { :sub_category => {}}}},
          {:product_gallery => {}}]),
          :status => @http_status
      }
    end
  end

  def new
    # @product = Product.new
  end

  def create

    @product = Product.new(product_params)
    if @product.save
      if params[:category]
        params[:category].each do |c|
          @category = ProductCategory.new
          @category.product_id = @product.id
          @category.category_id = c
          @category.save

        end

      end

      if params[:sub_category]
        params[:sub_category].each do |s|
          @subcategory = ProductSubcategory.new
          @subcategory.product_id = @product.id
          @subcategory.sub_category_id = s
          @subcategory.save
        end

      end
      if params[:gallery]
        params[:gallery].each do |g|
          @gallery = ProductGallery.new
          @gallery.product_id = @product.id
          @gallery.image = g
          @gallery.save
        end
      end

      if params[:tags]
        params[:tags].each do |t|
          @tag = ProductTag.new
          @tag.product_id = @product.id
          @tag.tag = t.downcase
          @tag.save
        end

      end

      success(@product)
    else
      validation_error(@product)
    end
    render :json => @http_response.to_json(:include => [
      {:product_tag => {}},
      {:product_categories => { :include => { :category => {:include => {:sub_categories => {}}}}}},
      {:product_subcategories => { :include => { :sub_category => {}}}},
      {:product_gallery => {}}]),
      :status => @http_status
  end

  def show
    if @product
      success(@product)
    end
    render :json => @http_response.to_json(:include => [
      {:product_tag => {}},
      {:product_categories => { :include => { :category => {:include => {:sub_categories => {}}}}}},
      {:product_subcategories => { :include => { :sub_category => {}}}},
      {:product_gallery => {}}]),
      :status => @http_status
  end

  def edit
    #
  end

  def update
    if @product
      if @product.update(product_params)
        if params[:category]
          params[:category].each do |c|
            ProductCategory.where(:product_id => @product.id, :category_id => c).first_or_create do |s|
              if !s
                s.product_id = @product.id
                s.category_id = s
                s.save

              end
            end

            delete = ProductCategory.where('category_id NOT IN (?)', params[:category]).where(:product_id => @product.id).destroy_all
          end

        end

        if params[:sub_category]
          params[:sub_category].each do |c|
            ProductSubcategory.where(:product_id => @product.id, :sub_category_id => c).first_or_create do |s|
              if !s
                s.product_id = @product.id
                s.sub_category_id = s
                s.save

              end
            end

            delete = ProductSubcategory.where('sub_category_id NOT IN (?)', params[:sub_category]).where(:product_id => @product.id).destroy_all

          end

        end

        if params[:tags]
          params[:tags].each do |c|
            ProductTag.where(:product_id => @product.id, :tag => c.downcase).first_or_create do |s|
              if !s
                s.product_id = @product.id
                s.tag = s
                s.save

              end
            end
            elete = ProductTag.where('tag NOT IN (?)', params[:tags]).where(:product_id => @product.id).destroy_all
          end

        end

        success(@product)
      else
        validation_error(@product)
      end
    end
    render :json => @http_response.to_json(:include => [
      {:product_tag => {}},
      {:product_categories => { :include => { :category => {:include => {:sub_categories => {}}}}}},
      {:product_subcategories => { :include => { :sub_category => {}}}},
      {:product_gallery => {}}]),
      :status => @http_status

  end

  def destroy
    if @product
      @product.destroy
      success_message("Product deleted")

    end

    render :json => @http_response, :status => @http_status

  end

  def multiple_delete
    ctr = 0
    no_id = ""
    params[:products][:id].each do |u|
      user = Product.find_by(:id => u)
      if user
        ctr = ctr + 1
      else
        no_id = no_id + " "+ u.to_s
      end
    end

    if params[:products][:id].count == ctr
      params[:products][:id].each do |u|
        user = Product.find_by(:id => u).destroy
      end
      success_message("Product/s deleted")
    else
      id = no_id.split(' ')
      ids = id.join(",")
      not_found("ID " + ids + " not found")
    end

    render :json => @http_response, :status => @http_status
  end

  def categories
    respond_to do |format|
      format.json {
        render json: { :categories => @categories }, status: 200
      }
    end
  end

  def tags
    respond_to do |format|
      format.html {

      }
      format.json {
        success(ProductTag.all)
        render :json => @http_response, :status => @http_status
      }
    end


  end


  def import
    file = params[:file]
    overwrite = params[:overwrite]
    options = {:key_mapping => {:unwanted_row => nil, :old_row_name => :new_name}}
    count = 0
    total_save = 0
    total_errors = 0
      begin
          n = SmarterCSV.process(file.path, options) do |array,index|
                count += 1
                # we're passing a block in, to process each resulting hash / =row (the block takes array of hashes)
                # when chunking is not enabled, there is only one hash in each array
                check_product = Product.find_by(:id => array[:id])
                if check_product && overwrite == "true"
                  puts "tralse"
                  total_save += 1 if  check_product.update(array)
                  if check_product.errors.any?
                    total_errors += 1
                    @http_response[:errors].push({:row => count, :message => check_product.errors.full_messages})
                  end

                else
                  product = Product.create( array.except(:id))
                  total_save += 1 if product.persisted?
                  if product.errors.any?
                    total_errors += 1
                    @http_response[:errors].push({:row => count, :message => product.errors.full_messages})
                  end
                end
          end

          # puts n.inspect
        rescue CSV::MalformedCSVError => ex
          total_errors += 1
          @http_response[:errors].push({:row => count, :message => "#{ex}"})

        rescue ActiveRecord::RecordNotUnique => ex
          total_errors += 1
          @http_response[:errors].push({:row => count, :message => "Duplicate entry for key 'PRIMARY"})

        rescue Exception => ex
          total_errors += 1
          @http_response[:errors].push({:row => count, :message => "#{ex}"})
            # render :json => @http_response.to_json(:message => "message"), :status => @http_status
      end
    @http_response[:results].append({:total_save =>total_save,:total_errors => total_errors})
    if total_errors == 0
      @http_status = 200
    end
    render :json => @http_response.to_json(:message => "message"), :status => @http_status
  end

  def export

      product_columns = Product.column_names
      products = Product.all.pluck.map { |id, sku,name, price, category_id, created_at, updated_at, image, in_stock, quantity, sub_category_id| {id: id,
                                                                                                                                                 sku: sku,
                                                                                                                                                 name: name,
                                                                                                                                                 price: price,
                                                                                                                                                 category_id: category_id,
                                                                                                                                                 created_at: created_at,
                                                                                                                                                 updated_at: updated_at,
                                                                                                                                                 image: image,
                                                                                                                                                 in_stock: in_stock,
                                                                                                                                                 quantity: quantity,
                                                                                                                                                 sub_category_id: sub_category_id}}
      # products = Product.all.map { |p| p.slice(:id, :image) }
      puts products.as_json
      puts product_columns
      @http_response[:results] = {:headers => product_columns, :data =>products}
      render :json => @http_response.to_json(:message => "message"), :status => @http_status = 200
  end
  private

  # strong parameters
  def product_params
    params.permit(
      :sku,
      :name,
      :description,
      :price,
      :image,
      :in_stock,
      :quantity,
      :discounted_price,
      :discount_date_start,
      :discount_date_end,
      :url,
      :weight,
      :unit
      )
  end

  def get_product
    begin
      @product = Product.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      not_found("Nothing Found")
    end
  end

  def get_categories
    @categories = Category.all
  end

end
