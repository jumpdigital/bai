class Api::Admin::OrdersController < ApplicationController
	respond_to :json
	include Api::ReturnHelper
	include Api::ModelSerializerHelper

	before_action :general_settings

	before_action :pass_model,
    :only => [
      :index,
      :create,
      :show,
      :update,
      :destroy,
			:validate_promo
    ]


  before_action :get_order,
    only: [
      :show,
      :edit,
      :update,
      :destroy,
			:cancel_order,
			:change_status
    ]

	def initialize
		@http_status = 400
		@http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
	end

	def pass_model
		authenticate_token(User)
	end

  def index
  	order = Order.all
  	success(order)


  	render :json => @http_response.to_json(:include => [ {:customer => {}},
			{:order_product => {:include => { :product => { :include => {:product_categories => {:include =>
																																													{:category => {}}},
																																	:product_subcategories => {:include =>
																																										{:sub_category => {}}}}}}}},
			{:order_history => { :include => { :order_status => {} }}}]), :status => @http_status

  end


	def create_order
		if !params[:promo_order].blank?
			params[:promo_order].each do |promo|
				if promo[:expired_at] > DateTime.now
					promorder = PromoOrder.where(:promo_id => promo[:id]).count #uses per coupon
					if  promo[:uses_per_coupon] != nil
						if promorder < promo[:uses_per_coupon].to_i
							if params[:customer_id]
								customer = Customer.find(params[:customer_id])
								customerpromo = customer.promo_orders.where(:promo_id => promo[:id]).count
								if promo[:uses_per_customer] != nil
									if customerpromo >= promo[:uses_per_customer].to_i
										promo_error("The promo code " + promo[:code] + " is already used.")
										return render :json => @http_response, :status => @http_status
									end
								end
							end
						else
							promo_error("The promo code " + promo[:code] + " is already used!")
							return render :json => @http_response, :status => @http_status
						end
					elsif promo[:uses_per_customer] != nil && promo[:uses_per_coupon] == nil
						if params[:customer_id]
							customer = Customer.find(params[:customer_id])
							customerpromo = customer.promo_orders.where(:promo_id => promo[:id]).count
							if customerpromo >= promo[:uses_per_customer].to_i
								promo_error("The promo code " + promo[:code] + " is already used.")
								return render :json => @http_response, :status => @http_status
							end
						end
					else
						puts "tang ina this"
					end
				elsif promo[:start_at]  > DateTime.now
					promo_error("The promo code " + promo[:code] + " can be use from " + promo[:start_at])
					return render :json => @http_response, :status => @http_status
				else
					promo_error("The promo code " + promo[:code] + " is already expired!")
					return render :json => @http_response, :status => @http_status
				end
			end
		end
		if params[:products]
			params[:products].each do |i|
				product = Product.find(i[:product_id])
				if i[:quantity].to_i > product.quantity
					order_error("The product " + product.name + " (" + product.sku + ") has only " + product.quantity.to_s + " more available!")
					return render :json => @http_response, :status => @http_status
				end
			end
		end
		total = 0
		order = Order.new(order_params)
		if order.valid?
			order.grand_total = params[:grand_total]#total
			order.save
		else
			validation_error(order)
			return render :json => @http_response, :status => @http_status
		end
		customer = get_customer(params[:customer_id], params[:email])
		params[:products].each do |p|
			order_product = OrderProduct.new
			product = nil
			product = Product.find(p[:product_id])
			order_product.product_id = p[:product_id]
			order_product.quantity = p[:quantity]
			order_product.total_price = product.price * p[:quantity].to_i
			if product.discount_date_start? && product.discount_date_end? && product.discounted_price?
				if product.discount_date_start <= DateTime.now && product.discount_date_end >= DateTime.now
					product_discount_price = (product.price - product.discounted_price)  * p[:quantity].to_i
					order_product.total_price = product_discount_price > 0 ? product_discount_price : 0
				end
			end
			total = total + order_product.total_price
			if order_product.valid?
				product.quantity = product.quantity.to_i - p[:quantity].to_i

				if product.quantity <= 0
					product.quantity = 0
					product.in_stock = 0
					product.save
				end
				product.save
				order_product.order_id = order.id
				order_product.save
			end
		end
		if(!params[:promo_order].blank? && order)
			params[:promo_order].each do |promo_order|
				PromoOrder.create(order_id: order.id, promo_id: promo_order[:id])
			end
		end
		order_status = OrderHistory.create([:order_id => order.id, :order_status_id => '2'])
		if params[:customer_id]
			OrderMailer.order_summary(order.reload, customer, @setting).deliver_now
		end
		success(order.reload)
		render :json => @http_response.to_json(:include => [ {:customer => {}},
			{:order_product => {:include => { :product => { :include => {:product_categories => {:include =>
																																													{:category => {}}},
																																	:product_subcategories => {:include =>
																																										{:sub_category => {}}}}}}}},
			{:order_history => { :include => { :order_status => {} }}}]), :status => @http_status
	end

	def create
		if params[:customer_id]
			find_customer = Customer.find_by(:id => params[:customer_id])
			if find_customer && find_customer.deactivate == nil
				process_order = create_order()
			else
				conflict("Need to activate the account before ordering!")
				render :json => @http_response, :status => @http_status
			end
		else
			process_order = create_order
		end
	end

	def show
		if @order
			success(@order)
		end
		render :json => @http_response.to_json(:include => [ {:customer => {}},{:shipping_zone_based_rate => {}},{:tax_setting => {}},
			{:promo_order => { :include => { :promo => { :include =>
																									{ :promo_selected => {},
																										:promo_type => {}}}}}},
			{:order_product => {:include => { :product => { :include => {:product_categories => {:include =>
																																													{:category => {}}},
																																	:product_subcategories => {:include =>
																																										{:sub_category => {}}}}}}}},
			{:dynamic_value => {}},
			{:order_history => { :include => { :order_status => {} }}}]), :status => @http_status
	end

	def filter
		result = Set.new
		orders = Order.includes(:customer, :order_history, :order_product)

		if params[:customer][:name].present?
			orders.each do |o|
				name = o.customer.first_name + " " + o.customer.middle_name + " " + o.customer.last_name
				puts "done"
				name = name.downcase
				params[:customer][:name].each do |n|
					n = n.downcase
					if !n.in? name
						result.delete(o)
						break
					end
					result << o
				end

			end
			orders = result
		end

		if params[:order_status].present?
			orders.each do |o|
					if params[:order_status] != o.order_history.order_status_id
						result.delete(o)
					else
						result << o
					end

			end
			orders = result
		end

		if params[:order_date][:range_date][0][:start_date].present?
			orders.each do |o|
				params[:order_date][:range_date].each do |d|
					start_date = Date.strptime(d[:start_date],"%m/%d/%Y")
					end_date = Date.strptime(d[:end_date],"%m/%d/%Y")
					case o.created_at.strftime("%m/%d/%Y")
					when start_date..end_date
						result << o
					else
						result.delete(o)
					end
				end

			end
			orders = result
		end


		if params[:order_total][:total].present?
			orders.each do |o|
				params[:order_total][:total].each do |t|
					if t != o.grand_total
						result.delete(o)
						break
					end
					result << o
				end

			end
			orders = result
		end


		if params[:order_id].present?
			orders.each do |o|
				substring = "-"
				if (!substring.in? params[:order_id]) && (o.parent_order == nil)
					if params[:order_id].to_i == o.id
						result << o
					else
						result.delete(o)
					end
				else
					if params[:order_id] == o.parent_order
						result << o
					else
						result.delete(o)
					end

				end
			end
			orders = result

		end




		success(orders)
		render :json => @http_response.to_json(:include => [ {:customer => {}},
			{:order_product => {:include => { :product => { :include => {:product_categories => {:include =>
																																													{:category => {}}},
																																	:product_subcategories => {:include =>
																																										{:sub_category => {}}}}}}}},
			{:order_history => { :include => { :order_status => {} }}}]), :status => @http_status
	end


	def update_order
		if !params[:promo_order].blank?
			params[:promo_order].each do |promo|
				if promo[:expired_at] > DateTime.now
					promorder = PromoOrder.where(:promo_id => promo[:id]).count #uses per coupon
					if  promo[:uses_per_coupon] != nil
						if promorder < promo[:uses_per_coupon].to_i
							if params[:customer_id]
								customer = Customer.find(params[:customer_id])
								customerpromo = customer.promo_orders.where(:promo_id => promo[:id]).count
								if promo[:uses_per_customer] != nil
									if customerpromo >= promo[:uses_per_customer].to_i
										promo_error("The promo code " + promo[:code] + " is already used.")
										return render :json => @http_response, :status => @http_status
									end
								end
							end
						else
							promo_error("The promo code " + promo[:code] + " is already used!")
							return render :json => @http_response, :status => @http_status
						end
					elsif promo[:uses_per_customer] != nil && promo[:uses_per_coupon] == nil
						if params[:customer_id]
							customer = Customer.find(params[:customer_id])
							customerpromo = customer.promo_orders.where(:promo_id => promo[:id]).count
							if customerpromo >= promo[:uses_per_customer].to_i
								promo_error("The promo code " + promo[:code] + " is already used.")
								return render :json => @http_response, :status => @http_status
							end
						end
					else
						puts "tang ina this"
					end
				elsif promo[:start_at]  > DateTime.now
					promo_error("The promo code " + promo[:code] + " can be use from " + promo[:start_at])
					return render :json => @http_response, :status => @http_status
				else
					promo_error("The promo code " + promo[:code] + " is already expired!")
					return render :json => @http_response, :status => @http_status
				end
			end
		end
		if params[:order_product]

			params[:order_product].each do |i|
				product = Product.find(i[:product_id])
				previous_order_product = OrderProduct.where(order_id: params[:id],product_id: i[:product_id]).last
				if previous_order_product
					if i[:quantity].to_i > (product.quantity + previous_order_product.quantity)
						order_error("The product " + product.name + " (" + product.sku + ") has only " + (product.quantity  + previous_order_product.quantity).to_s + " more available!")
						return render :json => @http_response, :status => @http_status
					end
				end
			end
		end
		total = 0
		if @order
			order = Order.new(order_params)
			if @order.parent_order == nil
				order.parent_order = "%06d" % @order.id + "-1"
			else
				ctr_order = @order.parent_order.partition('-').last.to_i + 1
				parent_order = @order.parent_order.partition('-').first
				order.parent_order = parent_order.to_s + "-" + ctr_order.to_s
			end
			if order.valid? && order.save
				params[:order_product].each do |p|
					order_product = OrderProduct.new
					product = Product.find(p[:product_id])
					order_product.product_id = p[:product_id]
					order_product.quantity = p[:quantity]
					order_product.total_price = product.price * p[:quantity].to_i
					if product.discount_date_start? && product.discount_date_end? && product.discounted_price?
						if product.discount_date_start <= DateTime.now && product.discount_date_end >= DateTime.now
							product_discount_price = (product.price - product.discounted_price)  * p[:quantity].to_i
							order_product.total_price = product_discount_price > 0 ? product_discount_price : 0
						end
					end
					total = total + order_product.total_price
					if order_product.valid?
						order.grand_total = params[:grand_total]
						order_product.order_id = order.id
						product.quantity = product.quantity - p[:quantity].to_i
						if product.quantity <= 0
							product.in_stock = 0
						end
						product.save
						order_product.save
					else
						validation_error(order_product)
						return render :json => @http_response, :status => @http_status
					end
				end
				previous_order_history = OrderHistory.where(order_id: params[:id]).last
				if cancel_status = OrderHistory.create([:order_id => order.id, :order_status_id => 2])
					if params[:customer_id]
						customer = get_customer(params[:customer_id], params[:email])
						OrderMailer.order_summary(order, customer, @setting).deliver_now
					end

					previous_order_history.order_status_id = 6
					previous_order_history.save
					puts "create"
				end
				order.save
				order.reload
				if(!params[:promo_order].blank?)
					params[:promo_order].each do |promo_order|
						# PromoOrder.create(order_id: previous_order_history.id, promo_id: promo_order[:id])
					 	PromoOrder.where(:order_id =>order.id, :promo_id => promo_order[:id]).first_or_create do |s|
		              		if 	!s
				                s.order_id = order.id
				                s.promo_id = promo_order[:id]
				                s.save
			             	 end
			            end
					end
					delete = PromoOrder.where('promo_id NOT IN (?)', params[:promo_order]).where(:order_id => params[:id]).destroy_all
				end
				if(params[:promo_order].blank?)
					promo_orders = PromoOrder.where(:order_id => params[:id])
					if !promo_orders.blank?
						delete = PromoOrder.where(:order_id => params[:id]).destroy_all
					end
				end
				success(order)
			else
				validation_error(order)
			end

		end
		render :json => @http_response.to_json(:include => [ {:customer => {}},
			{:promo_order => { :include => { :promo => { :include =>
																									{ :promo_selected => {},
																										:promo_type => {}}}}}},
			{:order_product => {:include => { :product => { :include => {:product_categories => {:include =>
																																													{:category => {}}},
																																	:product_subcategories => {:include =>
																																										{:sub_category => {}}}}}}}},
			{:order_history => { :include => { :order_status => {} }}}]), :status => @http_status
	end


	def update
		if params[:customer_id]
			find_customer = Customer.find_by(:id => params[:customer_id])
			if find_customer && find_customer.deactivate == nil
				process_order = update_order()
			else
				conflict("Need to activate the account before ordering!")
				render :json => @http_response, :status => @http_status
			end
		else
			process_order = update_order()
		end
	end

	def change_status
		status = OrderStatus.all
		if @order
			@next_status_id = @order.order_history.order_status_id + 1
			@order_history = OrderHistory.create([:order_id => @order.id, :order_status_id => @next_status_id])
			if @order_history
				if @next_status_id.to_s == status.select {|status| status.name == "processing"}.map(&:id)*","
					history = OrderHistory.where(:order_id => @order.id)
					pending_status = status.select {|status| status.name == "pending"}.map(&:id)*","
					last_history = history.select {|status| status.order_status_id == pending_status.to_i}*","
					if @order.customer_id
						OrderMailer.order_processing(@order, last_history, @setting).deliver_now
					end
				elsif @next_status_id.to_s == status.select {|status| status.name == "completed"}.map(&:id)*","
					history = OrderHistory.where(:order_id => @order.id)
					pending_status = status.select {|status| status.name == "pending"}.map(&:id)*","
					last_history = history.select {|status| status.order_status_id == pending_status.to_i}*","

					if params[:email]
						customer = get_customer(@order.customer_id, params[:email])
						@order.email = params[:email]
						@order.save
						OrderMailer.order_completed(@order, last_history, @setting, customer).deliver_now
					elsif @order.customer_id
						OrderMailer.order_completed(@order, last_history, @setting, @order.customer).deliver_now
					end


				end
				success(@order.reload)
			else
				validation_error(@order)
			end

		end
		render :json => @http_response.to_json(:include => [ {:customer => {}},
			{:order_product => {:include => { :product => { :include => {:product_categories => {:include =>
																																													{:category => {}}},
																																	:product_subcategories => {:include =>
																																										{:sub_category => {}}}}}}}},
			{:order_history => { :include => { :order_status => {} }}}]), :status => @http_status
	end

	def cancel_order
		cancel = OrderStatus.find_by(:name => 'cancelled')
		if @order
			@order.order_product.each do |product|
				search = Product.find(product.product_id)
				if search
					quantity = search.quantity
					search.quantity = quantity.to_i + product.quantity.to_i
					search.save

				end

			end
			@order_history = OrderHistory.create([:order_id => @order.id, :order_status_id => cancel.id])
			if @order.customer_id
				OrderMailer.order_cancel(@order, @setting).deliver_now
			end

			success(@order)

		end

		render :json => @http_response.to_json(:include => [ {:customer => {}},
			{:promo_order => { :include => { :promo => { :include =>
																									{ :promo_selected => {},
																										:promo_type => {}}}}}},
			{:order_product => {:include => { :product => { :include => {:product_categories => {:include =>
																																													{:category => {}}},
																																	:product_subcategories => {:include =>
																																										{:sub_category => {}}}}}}}},
			{:dynamic_value => {}},
			{:order_history => { :include => { :order_status => {} }}}]), :status => @http_status

	end

	def destroy
		if @order
			@order.destroy
			success_message("Order deleted")

		end

		render :json => @http_response, :status => @http_status

	end

	def all_orders
		order = Order.all
  	success(order)


		render :json => @http_response.to_json(:include => [ {:customer => {}},
			{:order_product => {:include => { :product => { :include => {:product_categories => {:include =>
																																													{:category => {}}},
																																	:product_subcategories => {:include =>
																																										{:sub_category => {}}}}}}}},
			{:order_history => { :include => { :order_status => {} }}}]), :status => @http_status

	end


	# def validate_promo
	# 	promo = Promo.find_by_code(params[:code])
	# 	datenow = DateTime.now
	# 	if promo
	# 		if promo.expired_at > datenow
	# 			promorder = PromoOrder.where(:promo_id => promo.id).count #uses per coupon
	# 			puts promo.uses_per_coupon
	# 			if promorder == promo.uses_per_coupon.to_i
	# 				if params[:customer_id] #uses per customer
	# 					customer = Customer.find(params[:customer_id])
	# 					promocustomer = PromoOrder.
	# 				end
	# 				not_found("Promo code is no longer available")
	#
	# 			else
	#
	# 				success(promo)
	# 			end
	# 		else
	# 			not_found("Promo code is already expired")
	# 		end
	#
	# 	else
	# 		not_found("Promo code is invalid")
	# 	end
	# 	render :json => @http_response.to_json(:include=> [{:promo_selected => {}}]), :status => @http_status
	# end

	def validate_promo
		datenow = DateTime.now
		promo = Promo.all
		discount = Array.new

		promo.each do |p|
			if p.expired_at > datenow
				promorder = PromoOrder.where(:promo_id => p.id).count #uses per coupon
				if promorder < p.uses_per_coupon.to_i
					if params[:customer_id]
						customer = Customer.find(params[:customer_id])
						customerpromo = customer.promo_orders.where(:promo_id => p.id).count
						puts customerpromo
						if customerpromo < p.uses_per_customer.to_i
							discount.push(p)
						end
					else
						discount.push(p)
					end

				end
			end
		end
		success(discount)
		render :json => @http_response.to_json(:include=> [{:promo_selected => {}}]), :status => @http_status

	end

	def dynamic_values
	    respond_to do |format|
	      format.html {

	      }
	      format.json {
	        success(DynamicValue.where(model: 'Order', key: 'shipping_method'))
	        render :json => @http_response, :status => @http_status
	      }
    end


  end

	def export
      	# customer_columns = %w{order_id
      	# 					  customer_name
      	# 					  payment_transaction_id
      	# 					  grand_total created_at
      	# 					  billing_address
      	# 					  billing_country
      	# 					  billing_city
      	# 					  billing_zip_code
      	# 					  shipping_address
      	# 					  shipping_country
							#   shipping_city
							#   shipping_zip_code
							#   ship_to
							# }
		column_names = ['Order #',
						'Customer name',
						'Order Status',
		 				'Ship to',
						'Grand total',
		 				'Billing address',
		 				'Billing country',
		 				'Billing city',
		 				'Billing zip code',
		 				'Shipping address',
		 				'Shipping country',
		 				'Shipping city',
		 				'Shipping zip code',
		 				'created_at',
		 				'updated_at']
		# Order.all.map {|c| {'Order #':( c.customer ? c.customer.first_name + " " + c.customer.last_name : nil) }}
      	orders = Order.all.map{|c| {'Order #': c.id,
  								    'Customer name':( c.customer ? c.customer.first_name + " " + c.customer.last_name : nil),
  								    'Order Status': c.order_history ? (c.order_history.order_status ? c.order_history.order_status.name : nil ) : nil ,
  								    'Ship to': c.ship_to,
                                    'Grand total': c.grand_total,
                                    'Billing address': c.billing_address,
                                    'Billing country': c.billing_country,
					 				'Billing city': c.billing_city,
					 				'Billing zip code': c.billing_zip_code,
					 				'Shipping address': c.shipping_address,
					 				'Shipping country': c.shipping_country,
					 				'Shipping city': c.shipping_city,
					 				'Shipping zip code': c.shipping_zip_code,
					 				'created_at': c.created_at,
					 				'updated_at': c.updated_at
						 			  }
						 		}
      	@http_response[:results] = {:headers => column_names, :data =>orders}

      	render :json => @http_response.to_json(:message => "message"), :status => @http_status = 200
  	end

		def get_customer(customer_id, email)
			customer_output = Hash.new
			if customer_id
				customer = Customer.find(customer_id)
				customer_output["email"] = customer.email
				customer_output["full_name"] = customer.first_name + " " + customer.last_name
				return customer_output

			else
				customer_output["email"] = email
				customer_output["full_name"] = "Valued Customer"
				return customer_output
			end


		end


	def graph
		sum = 0
		completed = OrderStatus.find_by(:name => 'completed')
		order = Order.all
		current_month = Date.today.month
		month_names = 5.downto(0).map { |m| m.months.ago.strftime("%B %Y") }
		sum_all = []

		month_names.each do |d|
			puts d
			order.each do |o|
				if o.order_history
					if o.order_history.order_status_id == completed.id
						if o.created_at.strftime("%B %Y") == d
							puts o.id
							sum += o.grand_total
						end

					end
				end
			end
			sum_all << sum
			sum = 0
		end

		success(sum_all)


		render :json => @http_response, :status => @http_status
	end

	def last_five_orders
		temp_order = Order.last(5)
		order = []
		temp_order.each do |o|
			order << order_last_five(o)
		end

		success(order)

		render :json => @http_response, :status => @http_status
	end
	private

	def get_order
    begin
      @order = Order.includes(:order_product).find(params[:id])
    rescue ActiveRecord::RecordNotFound
      not_found("Nothing Found")
    end
  end

	def order_params
		params.require(:order).permit(:customer_id, :payment_transaction_id, :grand_total, :promo_id, :parent_order, :billing_address, :billing_country, :billing_city, :billing_zip_code, :shipping_address, :shipping_country, :shipping_city, :shipping_zip_code, :status_id, :ship_to, :shipping_method, :totalPromo, :shipping_zone_based_rate_id, :tax_setting_id, :email)

	end

	def general_settings
		@setting = GeneralSetting.last
		return @setting

	end

end
