class Api::Admin::UsersController < ApplicationController
	respond_to :json
	include Api::ReturnHelper

	before_action :pass_model,
	 	:only => [
	 		:index,
	 		:create,
	 		:show,
	 		:update,
	 		:destroy,
	 		:logout,
	 		:change_password,
	 		:profile,
	 		:update_profile,
			:deactivate_activate
	 	]

  before_action :get_user,
		only: [
			:show,
			:edit,
			:update,
			:destroy,
			:deactivate_activate
		]

  before_action :get_roles,
		only: [
			:new,
			:create,
			:edit,
			:update,
      :roles
		]


	def initialize
		@http_status = 400
		@http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
	end

	def pass_model
		authenticate_token(User)
	end

	def login
		user = User.find_for_database_authentication(:email => params[:email])
		if user
			if user.valid_password?(params[:password])
				user.token = User.ensure_authentication_token
				if user.save
					success(user)
				else
					validation_error(user)
				end
			else
				conflict("Invalid Password")
			end
		else
			conflict("Invalid Email")
		end

		render :json => @http_response.to_json(:include => [{:role => {}}, {:general_setting => {}}]), :status => @http_status
	end

	def logout
		if @user
			@user.token = nil
			if @user.save
				success_message("Logout Sucessful")
			else
				validation_error(@user)
			end
			render :json => @http_response, :status => @http_status
		end


	end

	def change_password
		if @user
			if @user.valid_password?(params[:old_password])
				if params[:new_password] == params[:password_confirmation]
					@user.password = params[:new_password]
					if @user.save
						success(@user)
					else
						validation_error(@user)
					end
				else
					conflict("Password and Password Confirmation not match")
				end
			else
				conflict("Wrong Password")
			end
			render :json => @http_response, :status => @http_status
		end
	end

	def forgot_password
		@user = User.find_for_database_authentication(:email => params[:email])
		if @user.present?
			if @user.send_reset_password_instructions
				success_message("Email sent")
			else
				server_error("Could not send email for resetting password. Please retry later.")
			end
		else
			not_found('Invalid email ' + params[:email] )
		end
		render :json => @http_response, :status => @http_status
	end

	def reset_password
		 # @user = User.find_by_reset_password_token(params[:reset_password_token])
		 # render :template => 'devise/passwords/edit.html.erb'
		return redirect_to 'http://139.162.14.57/change-password/' + params[:reset_password_token]
	end

	def update_password
		user = User.find_by_reset_password_token(params[:reset_password_token])
		if user
			if params[:password] == params[:password_confirmation]
				user.password = params[:password]
				user.reset_password_token = nil
				if user.save
					success_message("ah")
					success(user)
				else
					validation_error(user)
				end
			else
				conflict("Password and Password Confirmation not match")
			end
		else
			unauthorized("Invalid token")
		end
		render :json => @http_response, :status => @http_status
	end

  def index
  	if @user
  		users = User.all
			success(users)
			render :json => @http_response.to_json(:include => [:role => {}]), :status => @http_status
  	end


  end

  def new
    #
  end

  def create
  	if @user
  		@user = User.new(user_params)
			@user.password = params[:password]
			@user.password_confirmation = params[:password_confirmation]
			if @user.save
				success(@user)
			else
				validation_error(@user)
			end
			render :json => @http_response.to_json(:include => [:role => {}]), :status => @http_status
  	end


  end

  def profile
  	if @user
  		success(@user)
  		render :json => @http_response.to_json(:include => [{:role => {}}]), :status => @http_status
  	end

  end

  def update_profile
  	if @user
  		if @user.update(user_params)
				success(@user)
			else
				validation_error(@user)
			end
			render :json => @http_response.to_json(:include => [{:role => {}}]), :status => @http_status
  	end



  end

  def show
  	if @user
  		if @find_user
  			success(@find_user)
  		end
  		render :json => @http_response.to_json(:include => [{:role => {}}]), :status => @http_status
  	end
  end

  def edit
    #
  end

  def update
  	if @user
  		if @find_user
				@find_user.password = params[:password]
				@find_user.password_confirmation = params[:password_confirmation]
  			if @find_user.update(user_params)

					success(@find_user)
				else
					validation_error(@find_user)
				end

  		end
  		render :json => @http_response.to_json(:include => [{:role => {}}]), :status => @http_status
  	end


  end

  def destroy
  	if @user
  		if @find_user
  			if @find_user.destroy
					success_message("User Deleted")
				else
					validation_error(@find_user)
				end
  		end
  		render :json => @http_response, :status => @http_status
  	end
  end

  def roles
    render json: @roles, status: 200
  end


	def deactivate_activate
		if @find_user
			@find_user.deactivate = params[:deactivate]
			if @find_user.save
				success(@find_user)
			else
				validation_error(@find_user)
			end
		end

		render :json => @http_response, :status => @http_status

	end

	def import
	    file = params[:file]
	    overwrite = params[:overwrite]
	    options = {:key_mapping => {:unwanted_row => nil, :old_row_name => :new_name}}
	    count = 0
	    total_save = 0
	    total_errors = 0
	      begin
	          n = SmarterCSV.process(file.path, options) do |array,index|
	                count += 1
	                # we're passing a block in, to process each resulting hash / =row (the block takes array of hashes)
	                # when chunking is not enabled, there is only one hash in each array
	                check_user = User.find_by(:id => array[:id])
	                if check_user && overwrite == "true"
	                  total_save += 1 if  check_user.update(array)
	                  if check_user.errors.any?
	                    total_errors += 1
	                    @http_response[:errors].push({:row => count, :message => check_user.errors.full_messages})
	                  end

	                else
	                  puts array
	                  user = User.new( array.except(:id))
	                  user.encrypted_password = array[:encrypted_password]
	                  user.skip_password_validation = true
	                  user.save
	                  total_save += 1 if user.persisted?
	                  if user.errors.any?
	                    total_errors += 1
	                    @http_response[:errors].push({:row => count, :message => user.errors.full_messages})
	                  end
	                end
	          end

	          # puts n.inspect
	        rescue CSV::MalformedCSVError => ex
	          total_errors += 1
	          @http_response[:errors].push({:row => count, :message => "#{ex}"})

	        rescue ActiveRecord::RecordNotUnique => ex
	          total_errors += 1
	          @http_response[:errors].push({:row => count, :message => "Duplicate entry for key 'PRIMARY"})

	        rescue Exception => ex
	          total_errors += 1
	          @http_response[:errors].push({:row => count, :message => "#{ex}"})
	            # render :json => @http_response.to_json(:message => "message"), :status => @http_status
	      end
	    @http_response[:results].append({:total_save =>total_save,:total_errors => total_errors})
	    if total_errors == 0
	      @http_status = 200
	    end
	    render :json => @http_response.to_json(:message => "message"), :status => @http_status
  	end

  	def export
      customer_columns = %w{id role_id email encrypted_password first_name middle_name last_name token created_at updated_at deactivate}
      customers = User.all.map{|c| {id: c.id,
                                 role_id: c.role_id,
                                 email: c.email,
                                 encrypted_password: c.encrypted_password,
                                 first_name: c.first_name,
                                 middle_name: c.middle_name,
                                 last_name: c.last_name,
                                 token: c.token,
                                 created_at: c.created_at,
                                 updated_at: c.updated_at,
                                 deactivate: c.deactivate,}}
      @http_response[:results] = {:headers => customer_columns, :data =>customers}
      render :json => @http_response.to_json(:message => "message"), :status => @http_status = 200
  	end

  private

  # strong parameters
	def user_params
		params.require(:user).permit(
      :role_id,
      :email,
			:password,
			:password_confirmation,
      :first_name,
      :middle_name,
      :last_name
			)
	end

	def get_user
		begin
			@find_user = User.find(params[:id])
		rescue ActiveRecord::RecordNotFound
			not_found("User not found")
		end
	end

  def get_roles
    @roles = Role.all
  end
end
