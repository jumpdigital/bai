class Api::Admin::SubCategoriesController < ApplicationController
  include Api::ReturnHelper
  layout false

  before_action :get_subcategory,
    only: [
      :show,
      :edit,
      :update,
      :destroy
    ]

  before_action :pass_model,
    :only => [
      :index,
      :create,
      :show,
      :update,
      :destroy
    ]

  def initialize
    @http_status = 400
    @http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
  end

  def pass_model
    authenticate_token(User)
  end

  def index
    respond_to do |format|
      format.html {

      }
      format.json {
        success(SubCategory.all)
        render :json => @http_response.to_json(:include => [{:category => {}}]), :status => @http_status
      }
    end
  end

  def create
    sub_category = SubCategory.new(sub_category_params)
    if sub_category.save
      success(sub_category)

    else
      validation_error(sub_category)
    end

    render :json => @http_response.to_json(:include => [{:category => {}}]), :status => @http_status

  end

  def edit

  end

  def show
    if @sub_category
      success(@sub_category)
    end

    render :json => @http_response.to_json(:include => [{:category => {}}]), :status => @http_status

  end

  def update
    if @sub_category
      if @sub_category.update(sub_category_params)
        success(@sub_category)
      else
        validation_error(@sub_category)
      end
    end
    render :json => @http_response.to_json(:include => [{:category => {}}]), :status => @http_status
  end

  def destroy
    if @sub_category
      if @sub_category.destroy
        success_message("Sub-category deleted")
      else
        validation_error(@sub_category)
      end
    end

    render :json => @http_response, :status => @http_status
  end

  def multiple_delete
    ctr = 0
    no_id = ""
    params[:subcategories][:id].each do |u|
      user = SubCategory.find_by(:id => u)
      if user
        ctr = ctr + 1
      else
        no_id = no_id + " "+ u.to_s
      end
    end

    if params[:subcategories][:id].count == ctr
      params[:subcategories][:id].each do |u|
        user = SubCategory.find_by(:id => u).destroy
      end
      success_message("Sub-category/s deleted")
    else
      id = no_id.split(' ')
      ids = id.join(",")
      not_found("ID " + ids + " not found")
    end

    render :json => @http_response, :status => @http_status
  end

  private
  def sub_category_params
    params.permit(
      :name,
      :description,
      :image,
      :category_id,
      :url

    )

  end

  def get_subcategory
    begin
      @sub_category = SubCategory.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      not_found("Nothing Found")
    end
  end

end
