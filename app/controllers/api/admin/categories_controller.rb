class Api::Admin::CategoriesController < ApplicationController
  include Api::ReturnHelper
  layout false

  before_action :pass_model,
    :only => [
      :index,
      :create,
      :show,
      :update,
      :destroy
    ]


  before_action :get_category,
    only: [
      :show,
      :edit,
      :update,
      :destroy
    ]

  def initialize
    @http_status = 400
    @http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
  end

  def pass_model
    authenticate_token(User)
  end

  def index
    respond_to do |format|
      format.html {

      }
      format.json {
        success(Category.all)
        render :json => @http_response, :status => @http_status
      }
    end
  end

  def new
    # @category = Category.new
  end

  def create
    @category = Category.new(category_params)

    if @category.save
      success(@category)
    else
      validation_error(@category)
    end
    render :json => @http_response, :status => @http_status
  end

  def show
    if @category
      success(@category)
    end
    render :json => @http_response, :status => @http_status
  end

  def edit
    #
  end

  def update
    if @category
      if @category.update(category_params)
        success(@category)
      else
        validation_error(@category)
      end
    end

    render :json => @http_response, :status => @http_status
  end

  def destroy
    if @category
      @category.destroy
      success_message("Category deleted")

    end

    render :json => @http_response, :status => @http_status

  end

  def multiple_delete
    ctr = 0
    no_id = ""
    params[:categories][:id].each do |u|
      puts u
      user = Category.find_by(:id => u)
      if user
        ctr = ctr + 1
      else
        no_id = no_id + " "+ u.to_s
      end
    end

    if params[:categories][:id].count == ctr
      params[:categories][:id].each do |u|
        user = Category.find_by(:id => u).destroy
      end
      success_message("Category deleted")
    else
      id = no_id.split(' ')
      ids = id.join(",")
      not_found("ID " + ids + " not found")
    end

    render :json => @http_response, :status => @http_status
  end

  private

  # strong parameters
  def category_params
    params.permit(
      :name,
      :description,
      :image,
      :url
      )
  end

  def get_category
    begin
      @category = Category.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      not_found("Nothing found.")
    end
  end

end
