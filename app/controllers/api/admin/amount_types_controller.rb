class Api::Admin::AmountTypesController < ApplicationController
  include Api::ReturnHelper
  layout false

  before_action :pass_model,
    :only => [
      :index
    ]


  def initialize
		@http_status = 400
		@http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
	end

  def pass_model
		authenticate_token(User)
	end

  def index
    respond_to do |format|
      format.html {

      }
      format.json {
        success(AmountType.all)
        return render :json => @http_response, :status => @http_status
      }
    end

  end
end
