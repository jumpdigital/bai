class Api::Admin::ShippingZoneController < ApplicationController
	 include Api::ReturnHelper
  layout false

     before_action :pass_model,
    :only => [
      :index,
      :create,
      :show,
      :update,
      :destroy
    ]
    before_action :get_shipping_zone,
    only: [
      :show,
      :edit,
      :update,
      :destroy,
    ]

  def initialize
		@http_status = 400
		@http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
	end

  def pass_model
		authenticate_token(User)
	end

  def index
    respond_to do |format|
      format.html {

      }
      format.json {
        success(ShippingZone.where(:shipping_setting_id => params[:shipping_setting_id]))
        render :json => @http_response.to_json(:include => [{:shipping_zone_country => { :include => { :tax_setting => {} }}},{:shipping_zone_based_rate => {}}]), :status => @http_status
      }
    end
  end
  
  def create
      # if params[:shipping_zone_country].blank?
      #   order_model("shipping_zone_country", "Shipping Zone Country can't be blank")
      #   return render :json => @http_response.to_json, :status => @http_status
      # end
      # if params[:shipping_zone_based_rate].blank?
      #   order_model("shipping_zone_based_rate", "Shipping Zone Based Rate can't be blank")
      #   return render :json => @http_response.to_json, :status => @http_status
      # end
      @shipping = ShippingZone.new(shipping_params)
      # @shipping.shipping_zone_country_attributes = params[:shipping_zone_country].map {|u| {:country => u[:country]}}
      # @shipping.shipping_zone_based_rate_attributes = params[:shipping_zone_based_rate].map {|u| {:name => u[:name],
      #                                                                                             :based_rate_type => u[:based_rate_type],
      #                                                                                             :minimum => u[:minimum],
      #                                                                                             :maximum => u[:maximum],
      #                                                                                             :is_free => u[:is_free],
      #                                                                                             :amount => u[:amount]}
      #                                                                                       }
      if @shipping.save
        @shipping.shipping_zone_country.each do |country|
          TaxSetting.create({:shipping_zone_country_id => country.id, :rate => 0})
        end
        success(@shipping)
      else
        validation_error(@shipping)
      end
      render :json => @http_response.to_json(:include => [{:shipping_zone_country => {}},{:shipping_zone_based_rate => {}}]), :status => @http_status
    
  end

  def show
    if @shipping_zone
      success(@shipping_zone)
    end
    render :json => @http_response.to_json(:include => [{:shipping_zone_country => {}},{:shipping_zone_based_rate => {}}]), :status => @http_status
  end

  def update
    if @shipping_zone
       puts shipping_params.inspect
       # @shipping_zone.shipping_zone_country_attributes = params[:shipping_zone_country].map {|u| {:country => u[:country],:id => u[:id],:_destroy => u[:_destroy]}}
       # @shipping_zone.shipping_zone_based_rate_attributes = params[:shipping_zone_based_rate].map {|u| {:id => u[:id],
       #                                                                                            :name => u[:name],
       #                                                                                            :based_rate_type => u[:based_rate_type],
       #                                                                                            :minimum => u[:minimum],
       #                                                                                            :maximum => u[:maximum],
       #                                                                                            :is_free => u[:is_free],
       #                                                                                            :amount => u[:amount],
       #                                                                                            :_destroy => u[:_destroy]}
       #                                                                                     }
      if @shipping_zone.update(shipping_params)
            success(@shipping_zone)
      else
        validation_error(@shipping_zone)
      end
    end
    render :json => @http_response.to_json(:include => [{:shipping_zone_country => {}},{:shipping_zone_based_rate => {}}]), :status => @http_status
  end

  private

  # strong parameters
  def shipping_params
    params.permit(
      :shipping_setting_id,
      :name,
      shipping_zone_country_attributes: [:id, :country,:_destroy],
      shipping_zone_based_rate_attributes: [:id, :shipping_zone_id,:based_rate_type,:name, :minimum,:maximum,:is_free,:amount,:_destroy ]
      )
  end
  def get_shipping_zone
    begin
      @shipping_zone = ShippingZone.find_by_id(params[:id])
    rescue ActiveRecord::RecordNotFound
      not_found("Shipping zone not found")
    end
  end

end
