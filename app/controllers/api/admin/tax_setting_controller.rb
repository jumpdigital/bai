class Api::Admin::TaxSettingController < ApplicationController
	include Api::ReturnHelper
  	layout false

 	before_action :pass_model,
    :only => [
      :index,
      :create,
      :show,
      :update,
      :destroy
    ]
    before_action :get_tax,
    only: [
      :show,
      :edit,
      :update,
      :destroy,
    ]

  def initialize
		@http_status = 400
		@http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
	end

  def pass_model
		authenticate_token(User)
	end

  def index
    respond_to do |format|
      format.html {

      }
      format.json {
        success(TaxSetting.find_by_id(params[:general_setting_id]))
        render :json => @http_response.to_json(:include => [{:tax_setting => {}}]), :status => @http_status
      }
    end
  end
  
  def create
      @shipping = TaxSetting.new(tax_params)
      if @shipping.save
        success(@shipping)
      else
        validation_error(@shipping)
      end
       render :json => @http_response.to_json(:include => [{:shipping_zone_country => {}}]), :status => @http_status
    
  end

  def show
    if @tax_setting
      success(@tax_setting)
    end
    render :json => @http_response.to_json(:include => [{:shipping_zone_country => {}}]), :status => @http_status
  end

  def update
    if @tax_setting
      if @tax_setting.update(tax_params)
            success(@tax_setting)
      else
        validation_error(@tax_setting)
      end
    end
     render :json => @http_response.to_json(:include => [{:shipping_zone_country => {}}]), :status => @http_status
  end

  private

  def tax_params
    params.permit(
      :tax_setting_country_id,
      :rate,
      )
  end
  def get_tax
    begin
      @tax_setting = TaxSetting.find_by_id(params[:id])
    rescue ActiveRecord::RecordNotFound
      not_found("Shipping zone not found")
    end
  end
end
