class Api::Admin::RolesController < ApplicationController
	respond_to :json
	include Api::ReturnHelper

	before_action :pass_model,
	 	:only => [
	 		:index
	 	]

	def initialize
		@http_status = 400
		@http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
	end

	def pass_model
		authenticate_token(User)
	end

	def index
		roles = Role.all
		success(roles)
		render :json => @http_response, :status => @http_status
	end

end