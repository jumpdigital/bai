class Api::Admin::ProductGalleriesController < ApplicationController
  include Api::ReturnHelper

  before_action :pass_model,
    :only => [
      :index,
      :create,
      :show,
      :update,
      :destroy
    ]

  before_action :get_gallery,
    only: [
      :show,
      :edit,
      :update,
      :destroy
    ]
    before_action :get_product,
      only: [
        :create
      ]

  def initialize
    @http_status = 400
    @http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
  end

  def pass_model
    authenticate_token(User)
  end

  def index
    respond_to do |format|
      format.html {

      }
      format.json {
        success(ProductGallery.all)
        render :json => @http_response, :status => @http_status
      }
    end

  end

  def create
    if @product
      if params[:gallery]
        params[:gallery].each do |g|
          @gallery = ProductGallery.new
          @gallery.product_id = @product.id
          @gallery.image = g
          @gallery.save
        end
      end

      success(@product)
    end
    render :json => @http_response.to_json(:include => [{:product_gallery => {}}]), :status => @http_status

  end

  def destroy
    if @gallery
      @gallery.destroy
      success_message("Image deleted")

    end
    render :json => @http_response, :status => @http_status
  end

  def multiple_delete
    ctr = 0
    no_id = ""
    params[:galleries][:id].each do |u|
      user = ProductGallery.find_by(:id => u)
      if user
        ctr = ctr + 1
      else
        no_id = no_id + " "+ u.to_s
      end
    end

    if params[:galleries][:id].count == ctr
      params[:galleries][:id].each do |u|
        user = ProductGallery.find_by(:id => u).destroy
      end
      success_message("Image/s deleted")
    else
      id = no_id.split(' ')
      ids = id.join(",")
      not_found("ID " + ids + " not found")
    end

    render :json => @http_response, :status => @http_status

  end

  private

  def get_product
    begin
      @product = Product.includes(:category).find(params[:product_id])
    rescue ActiveRecord::RecordNotFound
      not_found("Nothing Found")
    end
  end

  def get_gallery
    begin
      @gallery = ProductGallery.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      not_found("Nothing Found")
    end

  end
end
