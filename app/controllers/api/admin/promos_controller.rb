class Api::Admin::PromosController < ApplicationController
  include Api::ReturnHelper
  layout false

  before_action :get_promo,
    only: [
      :show,
      :edit,
      :update,
      :destroy
    ]

  before_action :pass_model,
    :only => [
      :index,
      :create,
      :show,
      :update,
      :destroy
    ]


  def initialize
		@http_status = 400
		@http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
	end

  def pass_model
		authenticate_token(User)
	end

  def index
    respond_to do |format|
      format.html {

      }
      format.json {
        success(Promo.all)
        return render :json => @http_response.to_json(:include => [
          {:amount_type => {}},
          {:promo_selected => {}},
          {:promo_type => {}}]), :status => @http_status
      }
    end

  end

  def create
    @promo = Promo.new(promo_params)
    if @promo.save
      if params[:selected]
        params[:selected].each do |p|
          @promo_selected = PromoSelected.new
          @promo_selected.promo_id = @promo.id
          @promo_selected.selected_id = p
          @promo_selected.save
        end
      end


      success(@promo)

    else
      validation_error(@promo)
    end

    return render :json => @http_response.to_json(:include => [
      {:amount_type => {}},
      {:promo_selected => {}},
      {:promo_type => {}}]), :status => @http_status
  end

  def update
    if @promo
      if @promo.update(promo_params)
        if params[:selected]
          params[:selected].each do |p|
            PromoSelected.where(:promo_id => @promo.id, :selected_id => p).first_or_create do |s|
              if !s
                s.promo_id = @promo.id
                s.selected_id = p
                s.save

              end
            end

            delete = PromoSelected.where('selected_id NOT IN (?)', params[:selected]).where(:promo_id => @promo.id).destroy_all
          end
        end

        success(@promo)
      else
        validation_error(@promo)
      end

    end
    return render :json => @http_response.to_json(:include => [
      {:amount_type => {}},
      {:promo_selected => {}},
      {:promo_type => {}}]), :status => @http_status
  end

  def show
    if @promo
      success(@promo)
    end

    return render :json => @http_response.to_json(:include => [
      {:amount_type => {}},
      {:promo_selected => {}},
      {:promo_type => {}}]), :status => @http_status
  end

  def destroy
    if @promo
      if @promo.promo_selected.destroy_all
        if @promo.destroy
          success_message("The promo has been deleted")
        else
          validation_error(@promo)
        end
      else
        validation_error(@promo.promo_selected)
      end

    end

    return render :json => @http_response, :status => @http_status

  end

  def multiple_delete
    ctr = 0
    no_id = ""
    params[:promo][:id].each do |u|
      user = Promo.find_by(:id => u)
      if user
        ctr = ctr + 1
      else
        no_id = no_id + " "+ u.to_s
      end
    end

    if params[:promo][:id].count == ctr
      params[:promo][:id].each do |u|
        user = Promo.find_by(:id => u).destroy
        selected = PromoSelected.where(:promo_id => u).destroy_all
      end
      success_message("Promo/s deleted")
    else
      id = no_id.split(' ')
      ids = id.join(",")
      not_found("ID " + ids + " not found")
    end

    render :json => @http_response, :status => @http_status

  end


  private

  def promo_params
    params.require(:promo).permit(
      :id,
      :name,
      :code,
      :description,
      :amount_type_id,
      :value,
      :uses_per_coupon,
      :uses_per_customer,
      :start_at,
      :expired_at,
      :status,
      :promo_type_id
    )

  end

  def get_promo
    begin
      @promo = Promo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      not_found("Nothing Found")
    end
  end
end
