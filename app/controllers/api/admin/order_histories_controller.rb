class Api::Admin::OrderHistoriesController < ApplicationController
  include Api::ReturnHelper

  before_action :pass_model,
    :only => [
      :update
    ]
  before_action :get_order_history,
    only: [
      :update
  ]

  def initialize
    @http_status = 400
    @http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
  end

  def pass_model
    authenticate_token(User)
  end

  def update
    if @history
      @history.order_status_id = params[:order_status]
      if @history.save
        success(@history)
      else
        validation_error(@history)
      end
    end

    render :json => @http_response.to_json(:include => [ :order_status => {}]), :status => @http_status
  end


  private

  def get_order_history
    begin
      @history = OrderHistory.includes(:order_status).find(params[:id])
    rescue ActiveRecord::RecordNotFound
      not_found("Nothing Found")
    end
  end
end
