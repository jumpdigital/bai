class Api::Admin::GeneralSettingsController < ApplicationController
  include Api::ReturnHelper
  layout false

     before_action :pass_model,
    :only => [
      :index,
      :create,
      :show,
      :update,
      :destroy
    ]
    before_action :get_user_general_setting,
    only: [
      :show,
      :edit,
      :update,
      :destroy,
    ]

  def initialize
		@http_status = 400
		@http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
	end

  def pass_model
		authenticate_token(User)
	end

  def index
    respond_to do |format|
      format.html {

      }
      format.json {
        success(GeneralSetting.all)
        return render :json => @http_response, :status => @http_status
      }
    end
  end
  
  def create
    @general = GeneralSetting.new(general_setting_params)
    @general.user_id = @user.id
    if @general.save
      success(@general)
    else
      validation_error(@general)
    end
    render :json => @http_response.to_json, :status => @http_status
  end

  def show
    if @general_setting
      success(@general_setting)
    end
    render :json => @http_response.to_json(:include => [{:user => {}}]), :status => @http_status
  end

  def update
    if @general_setting
      if @general_setting.update(general_setting_params)
        success(@general_setting)
      else
        validation_error(@general_setting)
      end
    end
    render :json => @http_response.to_json(:include => [{:user => {}}]), :status => @http_status
  end

  private

  # strong parameters
  def general_setting_params
    params.permit(
      :user_id,
      :store_name,
      :account_email,
      :store_logo,
      :legal_name_of_business,
      :phone,
      :street,
      :apt_suite,
      :timezone,
      :unit_system,
      :weight_unit,
      :currency,
      :city,
      :zip_code,
      :country,
      shipping_setting_attributes: [:id, :street, :apt_suite, :city, :zip_code,:country, :region, :phone]
      )
  end

  def get_user_general_setting
    begin
      @general_setting = GeneralSetting.find_by_user_id(@user.id)
    rescue ActiveRecord::RecordNotFound
      not_found("Setting not found")
    end
  end

end
