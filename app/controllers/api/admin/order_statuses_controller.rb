class Api::Admin::OrderStatusesController < ApplicationController
  include Api::ReturnHelper


  def initialize
    @http_status = 400
    @http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
  end

  def pass_model
    authenticate_token(User)
  end

  def index
    status = OrderStatus.all
    success(status)
    render :json => @http_response, :status => @http_status

  end
end
