class Api::Admin::CustomersController < ApplicationController
	respond_to :json
	include Api::ReturnHelper

	before_action :pass_model,
    :only => [
      :index,
      :create,
      :show,
      :update,
      :destroy,
			:deactivate_activate
    ]

  before_action :get_user,
		only: [
			:show,
			:edit,
			:update,
			:deactivate_activate
		]

  before_action :get_roles,
		only: [
			:new,
			:create,
			:edit,
			:update,
      :roles
		]

	def initialize
    @http_status = 400
    @http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
  end

  def pass_model
    authenticate_token(User)
  end

  def index
    customers = Customer.all
    success(customers)
    render :json => @http_response.to_json(:include => [ {:customer_billing_address => {}},
      {:customer_shipping_address => {}}
      ]), :status => @http_status
  end

  def new
    #
  end

  def create
		@user = Customer.new(customer_params)
		@user.password = params[:password]
		@user.password_confirmation = params[:password_confirmation]

		if @user.save
      @billing = CustomerBillingAddress.create([:customer_id => @user.id, :street_address => params[:billing_address], :country => params[:billing_country], :city => params[:billing_city], :zip_code => params[:billing_zip], :default => true])
      @shipping = CustomerShippingAddress.create([:customer_id => @user.id, :street_address => params[:shipping_address], :country => params[:shipping_country], :city => params[:shipping_city], :zip_code => params[:shipping_zip], :default => true])
      success(@user)

		else
			validation_error(@user)
		end

		render :json => @http_response.to_json(:include => [ {:customer_billing_address => {}},
      {:customer_shipping_address => {}}
      ]), :status => @http_status
  end

  def show
  	if @find_user
  		success(@find_user)
  	end
  	render :json => @http_response.to_json(:include => [ {:customer_billing_address => {}},
      {:customer_shipping_address => {}}
      ]), :status => @http_status
  end

  def edit
    #
  end

  def update
  	if @find_user
  		if @find_user.update(customer_params)
        @billling = CustomerBillingAddress.update(params[:customer_billing_address][:id],billing_params)
        @shipping = CustomerShippingAddress.update(params[:customer_shipping_address][:id],shipping_params)
				success(@find_user)
			else
				validation_error(@find_user)
			end
  	end
  	render :json => @http_response.to_json(:include => [ {:customer_billing_address => {}},
      {:customer_shipping_address => {}}
      ]), :status => @http_status

	end

  def destroy
    ctr = 0
    no_id = ""
    params[:customers].each do |u|
      puts u
      user = Customer.find_by(:id => u[:id])
      if user
        ctr = ctr + 1
      else
        no_id = no_id + " "+ u[:id].to_s
      end
    end

    if params[:customers].count == ctr
      params[:customers].each do |u|
        user = Customer.find_by(:id => u[:id]).destroy
      end
      success_message("Customer/s deleted")
    else
      id = no_id.split(' ')
      ids = id.join(",")
      not_found("ID " + ids + " not found")
    end

		render :json => @http_response, :status => @http_status
  end

  def roles
    render json: @roles, status: 200
  end

	def deactivate_activate
		if @find_user
			user = Customer.find(params[:id])
			user.deactivate = params[:deactivate]
			if user.save
				success(user)
			else
				validation_error(user)
			end
		end

		render :json => @http_response.to_json(:include => [ {:customer_billing_address => {}},
      {:customer_shipping_address => {}}
      ]), :status => @http_status

	end

  def import
    file = params[:file]
    overwrite = params[:overwrite]
    options = {:key_mapping => {:unwanted_row => nil, :old_row_name => :new_name}}
    count = 0
    total_save = 0
    total_errors = 0
      begin
          n = SmarterCSV.process(file.path, options) do |array,index|
                count += 1
                # we're passing a block in, to process each resulting hash / =row (the block takes array of hashes)
                # when chunking is not enabled, there is only one hash in each array
                check_customer = Customer.find_by(:id => array[:id])
                if check_customer && overwrite == "true"
                  puts "tralse"
                  total_save += 1 if  check_customer.update(array)
                  if check_customer.errors.any?
                    total_errors += 1
                    @http_response[:errors].push({:row => count, :message => check_customer.errors.full_messages})
                  end

                else
                  puts array
                  customer = Customer.new( array.except(:id))
                  customer.encrypted_password = array[:encrypted_password]
                  customer.skip_password_validation = true
                  customer.save
                  total_save += 1 if customer.persisted?
                  if customer.errors.any?
                    total_errors += 1
                    @http_response[:errors].push({:row => count, :message => customer.errors.full_messages})
                  end
                end
          end

          # puts n.inspect
        rescue CSV::MalformedCSVError => ex
          total_errors += 1
          @http_response[:errors].push({:row => count, :message => "#{ex}"})

        rescue ActiveRecord::RecordNotUnique => ex
          total_errors += 1
          @http_response[:errors].push({:row => count, :message => "Duplicate entry for key 'PRIMARY"})

        rescue Exception => ex
          total_errors += 1
          @http_response[:errors].push({:row => count, :message => "#{ex}"})
            # render :json => @http_response.to_json(:message => "message"), :status => @http_status
      end
    @http_response[:results].append({:total_save =>total_save,:total_errors => total_errors})
    if total_errors == 0
      @http_status = 200
    end
    render :json => @http_response.to_json(:message => "message"), :status => @http_status
  end

  def export

      customer_columns = %w{id role_id email birthdate encrypted_password first_name middle_name last_name mobile_number token created_at updated_at deactivate note}
      customers = Customer.all.map{|c| {id: c.id,
                                 role_id: c.role_id, 
                                 email: c.email,
                                 birthdate: c.birthdate,
                                 encrypted_password: c.encrypted_password,
                                 first_name: c.first_name,
                                 middle_name: c.middle_name,
                                 last_name: c.last_name,
                                 mobile_number: c.mobile_number,
                                 token: c.token,
                                 created_at: c.created_at,
                                 updated_at: c.updated_at,
                                 deactivate: c.deactivate,
                                 note: c.note}}
      @http_response[:results] = {:headers => customer_columns, :data =>customers}

      render :json => @http_response.to_json(:message => "message"), :status => @http_status = 200
  end

  private

  # strong parameters
	def customer_params
		params.require(:customer).permit(
      :id,
      :role_id,
      :email,
			:birthdate,
			:password,
			:password_confirmation,
      :first_name,
      :middle_name,
      :last_name,
      :mobile_number,
      :address1,
      :address2,
      :country,
      :city,
      :state,
      :zip,
      :token,
      :created_at,
      :updated_at,
			:deactivate,
			:note

			)
	end

  def billing_params
    params.require(:customer_billing_address).permit(
      :id,
      :customer_id,
      :street_address,
      :country,
      :city,
      :zip_code,
      :created_at,
      :updated_at

      )

  end

   def shipping_params
    params.require(:customer_shipping_address).permit(
      :id,
      :customer_id,
      :street_address,
      :country,
      :city,
      :zip_code,
      :created_at,
      :updated_at

      )

  end

	def get_user
		begin
			@find_user = Customer.find(params[:id])
		rescue ActiveRecord::RecordNotFound
			not_found("Nothing found.")
		end
	end

  def get_roles
    @roles = Role.all
  end
end
