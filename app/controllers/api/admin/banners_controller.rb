class Api::Admin::BannersController < ApplicationController
  include Api::ReturnHelper
  layout false


  before_action :pass_model,
    :only => [
      :index,
      :create,
      :show,
      :update,
      :destroy
    ]

  before_action :get_banner,
    :only => [
      :show,
      :update,
      :destroy
    ]


  def initialize
		@http_status = 400
		@http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
	end

  def pass_model
		authenticate_token(User)
	end

  def index
    banners = Banner.all
    success(banners)

    render :json => @http_response, :status => @http_status

  end

  def create
    banner = Banner.new(banner_params)
    if banner.save
      success(banner)
    else
      validation_error(banner)
    end

    render :json => @http_response, :status => @http_status

  end

  def show
    if @banner
      success(@banner)
    end

    render :json => @http_response, :status => @http_status
  end

  def update
    if @banner
      if banner = @banner.update(banner_params)
        success(@banner.reload)
      else
        validation_error(banner)
      end

    end
    render :json => @http_response, :status => @http_status
  end

  def destroy
    if @banner
      @banner.destroy
      success_message("Banner deleted")
    end

    render :json => @http_response, :status => @http_status

  end

  def multiple_delete
    ctr = 0
    no_id = ""
    params[:banners][:id].each do |u|
      user = Banner.find_by(:id => u)
      if user
        ctr = ctr + 1
      else
        no_id = no_id + " "+ u.to_s
      end
    end

    if params[:banners][:id].count == ctr
      params[:banners][:id].each do |u|
        user = Banner.find_by(:id => u).destroy
      end
      success_message("Banner deleted")
    else
      id = no_id.split(' ')
      ids = id.join(",")
      not_found("ID " + ids + " not found")
    end

    render :json => @http_response, :status => @http_status
  end

  private

  def banner_params
    params.permit(
      :name,
      :normal_photo,
      :retina_photo
      )

  end

  def get_banner
    begin
      @banner = Banner.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      not_found("Nothing Found")
    end

  end
end
