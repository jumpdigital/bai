class Api::CategoriesController < ApplicationController
  include Api::ReturnHelper
  respond_to :json

  def initialize
    @http_status = 400
    @http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
  end

  def index
    success(Category.all)
	  render :json => @http_response, :status => @http_status

	end


end
