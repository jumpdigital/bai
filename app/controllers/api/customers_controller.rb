class Api::CustomersController < ApplicationController

	include Api::ReturnHelper
	include Api::ModelSerializerHelper

	# skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }
	respond_to :json

	before_action :pass_model, :only => [:logout, :update, :show, :change_password]

	def initialize
    @http_status = 400
    @http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :errors => [] }
  end

	def pass_model
		authenticate_token(Customer)

	end

	def store
		customer = Customer.create(customer_params)
		customer.password = params[:password]
		if customer.save
			success(customer)
		else
			validation_error(customer)
		end
		render :json => @http_response.to_json(:include => [ {:customer_billing_address => {}},
      {:customer_shipping_address => {}}
      ]), :status => @http_status
	end


	def login
		customer = Customer.find_for_database_authentication(:email => params[:email])
		if customer
			if customer.valid_password?(params[:password])
				customer.token = Customer.ensure_authentication_token
				if customer.save
					logincustomer = customer_model(customer)
					success(logincustomer)
				else
					validation_error(customer)
				end
			else
				unauthorized("Invalid Password")
			end
		else
			unauthorized("Invalid Email")
		end



		# render json: a
		render json: @http_response, status: @http_status

	end

	def logout
		if @user
			@user.token = nil
			if @user.save
				success(@user)
			else
				validation_error(@user)
			end
		end

		render :json => @http_response, :status => @http_status
	end

	def update
		if @user
			if @user.update_attributes(customer_params)
				success(@user)
			else
				validation_error(@user)
			end
		end
		render :json => @http_response.to_json(:include => [ {:customer_billing_address => {}},
      {:customer_shipping_address => {}}
      ]), :status => @http_status
	end

	def show
		if @user
			success(@user)
		end


		render :json => @http_response.to_json(:include => [ {:customer_billing_address => {}},
      {:customer_shipping_address => {}}
      ]), :status => @http_status
	end

	def change_password
		if @user
			if @user.valid_password?(params[:old_password])
				if params[:new_password] == params[:password_confirmation]
					@user.password = params[:new_password]
					if @user.save
						success(@user)
					else
						validation_error(@user)
					end
				else
					conflict("Password and Password Confirmation not match")
				end
			else
				conflict("Wrong Password")
			end

		end




	end

	def generate_new_password_email
		customer = Customer.find_for_database_authentication(:email => params[:email])
		if customer.send_reset_password_instructions
			success(customer)
			render :json => @http_response, :status => @http_status
		end

	end



	protected

	def customer_params
		params.require(:customer).permit(:first_name,:middle_name, :last_name, :role_id, :mobile_number, :address1, :address2, :country, :city, :state, :zip, :email, :birthdate)
	end

end
