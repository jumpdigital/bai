class Admin::CategoriesController < ApplicationController

  layout false

  before_action :get_category,
    only: [
      :show,
      :edit,
      :update,
      :destroy
    ]

  def index
    respond_to do |format|
      format.html {

      }
      format.json {
        render json: { :categories => Category.all }, status: 200
      }
    end
  end

  def new
    # @category = Category.new
  end

  def create
    @category = Category.new(category_params)

    if @category.save
      render :json => { :message => "Category created." }, :status => 200
    else
      render :json => { :errors => @category.errors.full_messages }, :status => 422
    end
  end

  def show
    respond_to do |format|
      format.html {
      }
      format.json {
        render json: { :category => @category }, status: 200
      }
    end
  end

  def edit
    #
  end

  def update
    if @category.update(category_params)
      render :json => { :message => "Category updated." }, :status => 200
    else
      render :json => { :errors => @category.errors.full_messages }, :status => 422
    end
  end

  def destroy
    if @category.destroy
      render :json => { :message => "Category deleted." }, :status => 200
    else
      render :json => { :errors => @category.errors.full_messages }, :status => 422
    end
  end

  private

  # strong parameters
  def category_params
    params.require(:category).permit(
      :name,
      :description
      )
  end

  def get_category
    begin
      @category = Category.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render :json => "Nothing found.", status: 404
    end
  end

end
