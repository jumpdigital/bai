class Admin::ProductsController < ApplicationController

  layout false

  before_action :get_product,
    only: [
      :show,
      :edit,
      :update,
      :destroy
    ]

  before_action :get_categories,
    only: [
      :new,
      :create,
      :edit,
      :update,
      :categories
    ]

  def index
    respond_to do |format|
      format.html {

      }
      format.json {
        render :json => Product.all, include: :category , status: 200
      }
    end
  end

  def new
    # @product = Product.new
  end

  def create
    @product = Product.new(product_params)

    if @product.save
      render :json => { :message => "Product created." }, :status => 200
    else
      render :json => { :errors => @product.errors.full_messages }, :status => 422
    end
  end

  def show
    respond_to do |format|
      format.html {
      }
      format.json {
        render :json => @product.to_json(:include => :category), status: 200
      }
    end
  end

  def edit
    #
  end

  def update
    if @product.update(product_params)
      render :json => { :message => "Product updated." }, :status => 200
    else
      render :json => { :errors => @product.errors.full_messages }, :status => 422
    end
  end

  def destroy
    if @product.destroy
      render :json => { :message => "Product deleted." }, :status => 200
    else
      render :json => { :errors => @product.errors.full_messages }, :status => 422
    end
  end

  def categories
    respond_to do |format|
      format.json {
        render json: { :categories => @categories }, status: 200
      }
    end
  end

  private

  # strong parameters
  def product_params
    params.require(:product).permit(
      :category_id,
      :sku,
      :name,
      :description,
      :price
      )
  end

  def get_product
    begin
      @product = Product.includes(:category).find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render :json => "Nothing found.", status: 404
    end
  end

  def get_categories
    @categories = Category.all
  end

end
