class Admin::UsersController < ApplicationController

  layout false

  before_action :get_user,
		only: [
			:show,
			:edit,
			:update,
			:destroy
		]

  before_action :get_roles,
		only: [
			:new,
			:create,
			:edit,
			:update,
      :roles
		]

  def index
    respond_to do |format|
      format.html {

      }
      format.json {
        users = User.all
        render :json => users, include: :role, status: 200
      }
    end
  end

  def new
    # @user = User.new
  end

  def create
		@user = User.new(user_params)

		if @user.save
			render :json => { :message => "User created." }, :status => 200
		else
			render :json => { :errors => @user.errors.full_messages }, :status => 422
		end
  end

  def show
    respond_to do |format|
      format.html {
      }
      format.json {
        render :json => @user.to_json(:include => :role) , status: 200
      }
    end
  end

  def edit
    #
  end

  def update
		if @user.update(user_params)
			render :json => { :message => "User updated." }, :status => 200
		else
			render :json => { :errors => @user.errors.full_messages }, :status => 422
		end
  end

  def destroy
    if @user.destroy
			render :json => { :message => "User deleted." }, :status => 200
		else
			render :json => { :errors => @user.errors.full_messages }, :status => 422
    end
  end

  def roles
    respond_to do |format|
      format.json {
        render json: { :roles => @roles }, status: 200
      }
    end
  end

  private

  # strong parameters
	def user_params
		params.require(:user).permit(
      :role_id,
      :email,
			:password,
			:password_confirmation,
      :first_name,
      :middle_name,
      :last_name
			)
	end

	def get_user
		begin
			@user = User.find(params[:id])
		rescue ActiveRecord::RecordNotFound
			render :json => "Nothing found.", status: 404
		end
	end

  def get_roles
    @roles = Role.all
  end

end
