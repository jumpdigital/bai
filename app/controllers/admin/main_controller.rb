class Admin::MainController < ApplicationController

  def index
    render layout: "admin"
  end

  def dashboard
    render layout: false
  end
end
