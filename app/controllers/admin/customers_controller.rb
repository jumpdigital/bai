class Admin::CustomersController < ApplicationController

  layout false

  before_action :get_customer,
    only: [
      :show,
      :edit,
      :update,
      :destroy
    ]

  def index
    respond_to do |format|
      format.html {

      }
      format.json {
        customers = Customer.all
        render :json => customers, status: 200
      }
    end
  end

  def new
    # @customer = Customer.new
  end

  def create
    @customer = Customer.new(customer_params)

    if @customer.save
      render :json => { :message => "Customer created." }, :status => 200
    else
      render :json => { :errors => @customer.errors.full_messages }, :status => 422
    end
  end

  def show
    respond_to do |format|
      format.html {
      }
      format.json {
        render :json => @customer.to_json() , status: 200
      }
    end
  end

  def edit
    #
  end

  def update
    @customer.skip_reconfirmation!
    if @customer.update(customer_params)
      render :json => { :message => "Customer updated." }, :status => 200
    else
      render :json => { :errors => @customer.errors.full_messages }, :status => 422
    end
  end

  def destroy
    if @customer.destroy
      render :json => { :message => "Customer deleted." }, :status => 200
    else
      render :json => { :errors => @customer.errors.full_messages }, :status => 422
    end
  end

  private

  # strong parameters
  def customer_params
    params.require(:customer).permit(:first_name,:middle_name, :password, :last_name, :role_id, :mobile_number, :address1, :address2, :country, :city, :state, :zip, :email)
  end

  def get_customer
    begin
      @customer = Customer.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render :json => "Nothing found.", status: 404
    end
  end


end
