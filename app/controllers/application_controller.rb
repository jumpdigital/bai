class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session, :if => Proc.new { |c| c.request.format == 'application/json' }
  respond_to :html, :json

   before_filter :configure_permitted_parameters, if: :devise_controller?
   after_filter :set_csrf_cookie_for_ng
  #prepend_before_filter :get_auth_token, :if => Proc.new { |c| c.request.format == 'application/json' }
  # def angular
  # 	render 'layouts/application'
  # end


  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end
  private
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :middle_name, :address1, :country, :city])
    devise_parameter_sanitizer.permit(:account_update, keys: [:id, :first_name, :middle_name, :last_name, :mobile_number, :role_id, :address1, :address2, :state, :zip, :country, :city, :token, :created_at, :updated_at])
  end   

  def verified_request?
    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  end
end
