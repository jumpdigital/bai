class ShippingZoneBasedRate < ActiveRecord::Base
	belongs_to :shipping_zone
	validates :name,:based_rate_type,:minimum ,presence: true
end
