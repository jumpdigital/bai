class ShippingSetting < ActiveRecord::Base
	belongs_to :general_setting
	has_many :shipping_zone
end
