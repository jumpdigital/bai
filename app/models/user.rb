class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :role
  has_one :general_setting
  validates :first_name, :last_name, :role_id, presence: true
  attr_accessor :skip_password_validation
  
  def self.ensure_authentication_token
    token ||= generate_authentication_token
  end

  def self.generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless Customer.where(token: token).first
    end
  end

  def password_required?
    return false if skip_password_validation
    super
  end

end
