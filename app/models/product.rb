require 'csv'
class Product < ActiveRecord::Base
	include Api::ReturnHelper

	validates :sku, :name, :description, :price, presence: true
	validates :in_stock, inclusion: { in: [ true, false ] }
	validates :sku, uniqueness: true
	validates :name, uniqueness: true
	validates :url, uniqueness: true, allow_nil: true
	has_many :product_categories
	has_many :product_subcategories
	has_many :order_product
	has_many :product_gallery
	has_many :product_tag
	mount_uploader :image, ProductImageUploader
	# attr_accessible :sku, :name, :description, :price, :category_id, :created_at, :updated_at, :in_stock, :quantity, :sub_category_id

	def self.import(file)
		# product = Product.new
		# CSV.foreach(file.path, headers: true).with_index do |row, i|
		# 	# puts i
		#  	sku, name, price = row
	 #      	product_hash = row.to_h # exclude the price field
	 #      	product = Product.where(id: product_hash["id"])
	 #      	if product.count == 1
		#       	if product.first.update_attributes(product_hash.except("price"))
		#       		puts 'save'
		#       	else
	 #      		  	puts "#{sku} - #{product.errors.full_messages.join(",")}"
	 #      		  	# validation_error(product.errors.full_messages.join(",")) if product.errors.any?
	 #      		  	return if product.errors.any?
		#       	end

	 #      	else
		#         product = Product.create(product_hash)
		#         puts "#{sku} - #{product.errors.full_messages.join(",")}"
	 #        	# validation_error(new_product.errors.full_messages.join(",")) if new_product.errors.any?
	 #        	return product if product.errors.any?
	 #      	end # end if !product.nil?
	 #    end # end CSV.foreach
	 #    # render :json => @http_response.to_json(:message => "message"), :status => @http_status
	 #    return product
	 	# 	begin
		 #   		options = {:headers_in_file => true, :row_sep => :auto}
			# 	n = SmarterCSV.process(file.path, options) do |chunk|
			# 	      # we're passing a block in, to process each resulting hash / row (block takes array of hashes)
			# 	      # when chunking is enabled, there are up to :chunk_size hashes in each chunk
			# 	      Product.create( chunk.first )   # insert up to 100 records at a time
			# 	      puts chunk
			# 	end
			# 	puts n
			# rescue Exception => e
			# 	puts e
			# end
		options = {:chunk_size => 100,:key_mapping => {:unwanted_row => nil, :old_row_name => :new_name}}
		begin
			n = SmarterCSV.process(file.path, options) do |array|
			      # we're passing a block in, to process each resulting hash / =row (the block takes array of hashes)
			      # when chunking is not enabled, there is only one hash in each array
			      product = Product.create( array.first.except(:id) )
			      puts product.errors.full_messages if product.errors.any?
			end
			puts n.inspect
			return "sucess"
		rescue CSV::MalformedCSVError => e
			puts e
			return e
		rescue Exception => e
			puts e
			return e
		# rescue ActiveRecord::RecordNotUnique => exception
		# 	puts exception.error
		end
	end
end
