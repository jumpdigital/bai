class SubCategory < ActiveRecord::Base
  validates :name, uniqueness: true
  validates :url, uniqueness: true

  mount_uploader :image, ProductImageUploader
  belongs_to :category
  has_many :product_subcategories
end
