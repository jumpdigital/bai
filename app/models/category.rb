class Category < ActiveRecord::Base
	has_many :products
	validates :name, presence: true
	validates :name, uniqueness: true
	validates :url, uniqueness: true
	mount_uploader :image, ProductImageUploader
	has_many :sub_categories
	has_many :product_categories
end
