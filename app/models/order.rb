class Order < ActiveRecord::Base
	belongs_to :customer

	has_many :order_product
	has_many :promo_order
	has_many :products, through: :order_product
	has_one :order_history, -> {  order('ID desc'). limit 1 }
	has_one :payment_transaction
	belongs_to :tax_setting
	belongs_to :shipping_zone_based_rate
	validates_associated :order_product
	belongs_to :dynamic_value, foreign_key: "shipping_method"
	# validates :grand_total, presence: true

	def self.save_to_cart(customer_id,product_id, quantity)
    order = Order.new
      order.customer_id = customer_id
      if order.save
        OrderHistory.create([:order_id => order.id, :order_status_id => 1])
        order_product = OrderProduct.save_order_product(order.id, product_id, quantity)
      else
				#
      end
	end
end
