class Customer < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  validates :first_name, :last_name, :birthdate, presence: true
  has_many :orders, -> {  order('ID desc'). limit 1 }
  has_many :order_histories, through: :order
  has_one :customer_billing_address
  has_one :customer_shipping_address

  has_many :orders
  has_many :promo_orders, :through  => :orders

  attr_accessor :skip_password_validation

  #token

  def self.ensure_authentication_token
    token ||= generate_authentication_token
  end

  def self.generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless Customer.where(token: token).first
    end
  end

  # get customer's cart
  # return nil if no cart
  def getCart
    orders = self.orders

    cart = nil

    cart_order_status = OrderStatus.where(name: 'cart').first

    if !orders.blank?
      orders.each do |order|
        if !order.order_histories.blank?
          if order.order_histories.count == 1
            # this order must be the cart
            if order.order_histories.first.order_status_id = cart_order_status.id
              cart = order
            end
          end
        end
      end
    end

    return cart
  end

  # get or set cart if none
  def getCartOrSet
    cart = self.getCart

    cart_order_status = OrderStatus.where(name: 'cart').first

    if cart == nil
      # create cart
      order = Order.new({
          customer_id: self.id,
          grand_total: 0
        })
      order.save

      order_history = OrderHistory.new({
          order_id: order.id,
          order_status_id: cart_order_status.id
        })
      order_history.save

      cart = order
    end

    return cart
  end

  protected

  def password_required?
    return false if skip_password_validation
    super
  end
end
