class GeneralSetting < ActiveRecord::Base
	has_one :shipping_setting
	has_many :shipping_zone, through: :shipping_setting
	has_many :shipping_zone_based_rate, through: :shipping_zone
  	accepts_nested_attributes_for :shipping_setting, :shipping_zone
	validates :store_name, :account_email, :store_logo, :timezone, :unit_system, :weight_unit, :currency, presence: true

	mount_uploader :store_logo, ProductImageUploader
	belongs_to :user
end
