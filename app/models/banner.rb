class Banner < ActiveRecord::Base
  mount_uploader :normal_photo, ProductImageUploader
  mount_uploader :retina_photo, ProductImageUploader
  validates :name, presence: true
  validates :name, uniqueness: true
end
