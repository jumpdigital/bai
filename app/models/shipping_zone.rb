class ShippingZone < ActiveRecord::Base
	has_many :shipping_zone_country
	has_many :shipping_zone_based_rate
	belongs_to :shipping_setting
	belongs_to :general_setting
  	validates :shipping_zone_country, :shipping_zone_based_rate, :shipping_setting_id, presence: true
	validates :name, uniqueness: true

	accepts_nested_attributes_for :shipping_zone_country, :shipping_zone_based_rate, :allow_destroy => true
  	# def self.validateShipiingCountry(params)

  	# end
end
