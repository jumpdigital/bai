class OrderProduct < ActiveRecord::Base
	belongs_to :product
	belongs_to :order

	# validates :order_id, presence: true
	validates :product_id, presence: true
	validates :quantity, presence: true
	validates :quantity, numericality: true, :if => "!quantity.blank?"
	validates :total_price, presence: true, numericality: true

	def self.save_order_product(order_id, product_id, quantity)
		order_product = OrderProduct.find_by(:order_id => order_id, :product_id => product_id)
		if order_product
			order_product.quantity = quantity
			order_product.save
		else
			order_product = OrderProduct.create([:order_id => order_id, :product_id=> product_id, :quantity => quantity])
		end
	end
end
