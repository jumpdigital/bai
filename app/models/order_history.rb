class OrderHistory < ActiveRecord::Base
	belongs_to :order
	belongs_to :order_status

	# select completed sales
	scope :complete, -> { where(status: "complete") }

	# select cart orders only
	scope :cart, -> {
		cart = OrderStatus.where(name: "cart").first
		where(order_status_id: cart.id)
	}
end
