class Promo < ActiveRecord::Base
  has_many :promo_selected
  belongs_to :promo_type
  belongs_to :amount_type
end
