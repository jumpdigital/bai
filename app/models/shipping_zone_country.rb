class ShippingZoneCountry < ActiveRecord::Base
	belongs_to :shipping_zone
	has_one :tax_setting
	validates :country, presence: true
end
