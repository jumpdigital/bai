class PaymentTransaction < ActiveRecord::Base
  has_one :payment_method
  has_one :payment_method_type, :through => :payment_method
  has_many :paypal_transactions
  has_many :credit_card_transactions
end
