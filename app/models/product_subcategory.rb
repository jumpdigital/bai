class ProductSubcategory < ActiveRecord::Base
  belongs_to :product
  belongs_to :sub_category
end
