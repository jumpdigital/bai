angular.module('bai')
.controller('CustomerProfileCtrl', [
'$scope',
'$state',
'Auth',
function($scope, $state, Auth){
  Auth.currentUser().then(function (customer){
    $scope.customer = customer;
  });
   $scope.update = function() {
    Auth.updateUser($scope.customer).then(function(){
      $state.go('profile', {}, { reload: true });
      $scope.customer.current_password = null;
    }, function errorCallback(response) {
      $scope.errorMessage = response.data.errors;
       console.log(response.data.errors);
    });

  };
}]);