angular.module('bai').controller('ProductCtrl', [
'$scope',
'products',

function($scope,products){
	$scope.products = products

	$scope.search = function(name) {
		if($scope.productname === '') { return; }
		products.searchProduct( {
		    name: $scope.productname
		});
	};

	$scope.clearSearch = function(){
		products.clearSearch();
		$scope.productname = null;
	};

	$scope.addToCart = function(product_id, quantity){
		products.addToCart ({
			product_id: product_id,
			quantity: quantity

		});
		console.log(quantity)
	}
}])