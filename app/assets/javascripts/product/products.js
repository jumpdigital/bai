angular.module('bai').factory('products', ['$http', function($http){
  var o = [];

  var clear = [];

  o.getProducts = function() {
  	$http.get('/productlist.json')
  	.then(
  		function onSuccess(response){
  			angular.extend(o,response.data);
  			angular.extend(clear, response.data);
  		},
  		function onFailure(response){
  			console.log(response);
  		}
  	);
  };


  o.searchProduct = function(name){
  	o.length = 0;
  	$http.post('/products.json',name)
  	.then(
  		function onSuccess(response){
  			o.push(response.data);
  		},
  		function onFailure(response){
  			// console.log("failed").
  		}
	);
  };


  o.clearSearch = function() {
  	angular.extend(o, clear);

  };
	

  o.showProduct = function(id) {
  	o.length = 0;
  	$http.get('/product/' + id + '.json')
  	.then(
  		function onSuccess(response){
  			o.push(response.data);
  		},
  		function onFailure(response){

  		}
  	);
  };


  o.addToCart = function(product_id, quantity){
  	$http.post('/order.json', product_id, quantity)
  	.then (
  		function onSuccess(response){
  			
  		},
  		function onFailure(response){
  			
  		}
  	);
  };
  return o;
}])
