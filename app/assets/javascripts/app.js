angular.module('bai', ['ui.router', 'templates', 'Devise']).config([
'$stateProvider',
'$urlRouterProvider',
'$locationProvider',
function($stateProvider, $urlRouterProvider,$locationProvider, $stateParams) {
  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'home/_home.html',
      controller: 'MainCtrl',
      })

    .state('login', {
      url: '/login',
      templateUrl: 'auth/_login.html',
      controller: 'AuthCtrl',
      onEnter: ['$state', 'Auth', function($state, Auth) {
        Auth.currentUser().then(function(){
          console.log(Auth.currentUser());
          $state.go('home');
        })
      }]
    })
    .state('register', {
      url: '/register',
      templateUrl: 'auth/_register.html',
      controller: 'AuthCtrl',
      onEnter: ['$state', 'Auth', function($state, Auth) {
        Auth.currentUser().then(function (){
          $state.go('home');
        })
      }]
    })
    .state('reset', {
      url: '/customers/password/edit?reset_password_token={token}',
      templateUrl: 'auth/_reset_password.html',
      controller: 'AuthCtrl',
    })

    .state('profile', {
      url: '/profile',
      templateUrl: 'customer/_customer_profile.html',
      controller: 'CustomerProfileCtrl',
      })

     .state('profile_edit', {
      url: '/profile/edit',
      templateUrl: 'customer/_customer_profile_edit.html',
      controller: 'CustomerProfileCtrl',
      })

     .state('forgotpassword', {
      url: '/recover',
      templateUrl: 'auth/_forgot_password.html',
      controller: 'AuthCtrl',
      })

     .state('products',{
        url: '/products',
        templateUrl: 'customer/_products.html',
        controller: 'ProductCtrl',
        resolve: {
          postPromise: ['products',
            function(products){
              return products.getProducts();
            }
          ]}

     })

     .state('show_product',{
        url: '/products/{id}',
        templateUrl: 'customer/_showProduct.html',
        controller: 'ProductCtrl',
        resolve: {
          get: ['$stateParams','products',
            function($stateParams, products){
              return products.showProduct($stateParams.id);
            }
          ]}

     })

     
     .state('orders',{
        url: '/orders',
        templateUrl: 'customer/_cart.html',
        controller: 'OrderCtrl',
        resolve: {
          postPromise: ['orders',
            function(orders){
              return orders.getCart();
            }
          ]}

     });

  //$urlRouterProvider.html5Mode(true)
    $urlRouterProvider.otherwise('home');
    $locationProvider.html5Mode(true);

}]).run(function ($browser) {
    $browser.baseHref = function () { return "/" };
  });;
