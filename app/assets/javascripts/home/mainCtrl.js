angular.module('bai').controller('MainCtrl', [
'$scope',
'Auth',
function($scope,Auth){
	 Auth.currentUser().then(function (customer){
    $scope.customer = customer;
  });
	$scope.signedIn = Auth.isAuthenticated;
  $scope.test = 'Hello world!';
}])