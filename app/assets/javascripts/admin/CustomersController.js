/**
 * CustomersController
 */
(function(){

  baiApp.controller('CustomersController', [
    '$scope',
    '$http',
    '$location',
    '$routeParams',

    function($scope, $http, $location, $routeParams) {

    $scope.message = 'Welcome to the Customers page!';

    $scope.customers = null;

    $scope.customer = null;

   // $scope.roles = null;

    $scope.errors = null;

    // handle redundant $http promises
    var onGetCustomersSuccess = function(response){
      $scope.customers = response.data;
    };
    var onGetCustomersFailure = function(response){
      $scope.errors = response.data.errors;
    };
    var onGetCustomerSuccess = function(response){
      $scope.customer = response.data;
    };
    var onGetCustomerFailure = function(response){
      $scope.errors = response.data.errors;
    };

    // CRUD functions

    // get customers
    $scope.getCustomers = function()
    {
      $scope.customers = $http.get('/admin/customers.json')
        .then(onGetCustomersSuccess, onGetCustomersFailure);
    }

    // show customer
    $scope.getCustomer = function()
    {
      $scope.customer = $http.get('/admin/customers/' + $routeParams.id + '.json')
        .then(onGetCustomerSuccess, onGetCustomerFailure);
    };

    // new customer
    $scope.newCustomer = function()
    {
    //  $scope.roles = $scope.getRoles();
    };

    // create customer
    $scope.createCustomer = function(customer)
    {
      var data = {
        customer: customer
      }

      $http.post('/admin/customers', data)
        .then(
          function onSucces(response){
            $location.path("/customers");
          },
          function onFailure(response){
            $scope.errors = response.data.errors;
            console.log(response.data);
          }
        );
    };

    // edit customer
    $scope.editCustomer = function()
    {
      $scope.customer = $scope.getCustomer();
     // $scope.roles = $scope.getRoles();
    };

    // update customer
    $scope.updateCustomer = function(customer)
    {
      var data = {
        customer: customer
      }

      $http.patch('/admin/customers/' + customer.id, data)
        .then(
          function onSuccess(response)
          {
            $location.path("/customers/" + $scope.customer.id);
            $scope.customer = response.data.customer;
          }
          ,
          function onFailure(response)
          {
            $scope.errors = response.data.errors;
          }
        );
    };

    // delete customer
    $scope.deleteCustomer = function(customer)
    {
      $http.delete('/admin/customers/' + customer.id)
        .then(
          function onSuccess(response){
            // remove customer from customers
            $scope.customers.splice( $scope.customers.indexOf(customer), 1 );
          },
          function onFailure(response){
            $scope.errors = response.data.errors;
          }
        );
    };

    // get roles
    // $scope.getRoles = function()
    // {
    //   $http.get('/admin/customers/roles.json')
    //     .then(
    //       function onSuccess(response){
    //         $scope.roles = response.data.roles;
    //       },
    //       function onFailure(response){
    //         // restrict creation of new customer when no roles
    //         $location.path("/customers");
    //       }
    //     );
    // };

  }]);

})(); // end IIFE
