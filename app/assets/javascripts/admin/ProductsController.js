/**
 * ProductsController
 */
(function(){

  baiApp.controller('ProductsController', [
    '$scope',
    '$http',
    '$location',
    '$routeParams',

    function($scope, $http, $location, $routeParams) {

    $scope.message = 'Welcome to the Products page!';

    $scope.products = null;

    $scope.product = null;

    $scope.categories = null;

    $scope.errors = null;

    // handle redundant $http promises
    var onGetProductsSuccess = function(response){
      $scope.products = response.data;
  	};
  	var onGetProductsFailure = function(response){
  		$scope.errors = response.data.errors;
  	};
    var onGetProductSuccess = function(response){
      $scope.product = response.data;
  	};
  	var onGetProductFailure = function(response){
  		$scope.errors = response.data.errors;
  	};

    // CRUD functions

    // get products
    $scope.getProducts = function()
    {
      $scope.products = $http.get('/admin/products.json')
        .then(onGetProductsSuccess, onGetProductsFailure);
    }

    // show product
    $scope.getProduct = function()
    {
      $scope.product = $http.get('/admin/products/' + $routeParams.id + '.json')
        .then(onGetProductSuccess, onGetProductFailure);
    };

    // new product
    $scope.newProduct = function()
    {
      $scope.categories = $scope.getCategories();
    };

    // create product
    $scope.createProduct = function(product)
    {
      var data = {
        product: product
      }

      $http.post('/admin/products', data)
    		.then(
          function onSucces(response){
            $location.path("/products");
          },
          function onFailure(response){
            $scope.errors = response.data.errors;
          }
        );
    };

    // edit product
    $scope.editProduct = function()
    {
      $scope.product = $scope.getProduct();
      $scope.categories = $scope.getCategories();
    };

    // update product
    $scope.updateProduct = function(product)
    {
      var data = {
        product: product
      }

      $http.patch('/admin/products/' + product.id, data)
    		.then(
          function onSuccess(response)
          {
            $location.path("/products/" + $scope.product.id);
            $scope.product = response.data.product;
          }
          ,
          function onFailure(response)
          {
            $scope.errors = response.data.errors;
          }
        );
    };

    // delete product
    $scope.deleteProduct = function(product)
    {
      $http.delete('/admin/products/' + product.id)
    		.then(
          function onSuccess(response){
            // remove product from products
            $scope.products.splice( $scope.products.indexOf(product), 1 );
          },
          function onFailure(response){
            $scope.errors = response.data.errors;
          }
        );
    };

    // get categories
    $scope.getCategories = function()
    {
      $http.get('/admin/categories.json')
    		.then(
          function onSuccess(response){
            $scope.categories = response.data.categories;
          },
          function onFailure(response){
            // restrict creation of new product with no categories
            $location.path("/products");
          }
        );
    };

  }]);

})(); // end IIFE
