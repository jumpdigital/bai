/**
 * CategoriesController
 */
(function(){

  baiApp.controller('CategoriesController', [
    '$scope',
    '$http',
    '$location',
    '$routeParams',

    function($scope, $http, $location, $routeParams) {

    $scope.message = 'Welcome to the Categories page!';

    $scope.categories = null;

    $scope.category = null;

    $scope.errors = null;

    // handle redundant $http promises
    var onGetCategoriesSuccess = function(response){
      $scope.categories = response.data.categories;
  	};
  	var onGetCategoriesFailure = function(response){
  		$scope.errors = response.data.categories.errors;
  	};
    var onGetCategorySuccess = function(response){
      $scope.category = response.data.category;
  	};
  	var onGetCategoryFailure = function(response){
  		$scope.errors = response.data.category.errors;
  	};

    // CRUD functions

    // get categories
    $scope.getCategories = function()
    {
      $scope.categories = $http.get('/admin/categories.json')
        .then(onGetCategoriesSuccess, onGetCategoriesFailure);
    }

    // show category
    $scope.getCategory = function()
    {
      $scope.category = $http.get('/admin/categories/' + $routeParams.id + '.json')
        .then(onGetCategorySuccess, onGetCategoryFailure);
    };

    // new category
    $scope.newCategory = function()
    {

    };

    // create category
    $scope.createCategory = function(category)
    {
      var data = {
        category: category
      }

      $http.post('/admin/categories', data)
    		.then(
          function onSucces(response){
            $location.path("/categories");
          },
          function onFailure(response){
            $scope.errors = response.data.errors;
          }
        );
    };

    // edit category
    $scope.editCategory = function()
    {
      $scope.category = $scope.getCategory();
    };

    // update category
    $scope.updateCategory = function(category)
    {
      var data = {
        category: category
      }

      $http.patch('/admin/categories/' + category.id, data)
    		.then(
          function onSuccess(response)
          {
            $location.path("/categories/" + $scope.category.id);
            $scope.category = response.data.category;
          }
          ,
          function onFailure(response)
          {
            $scope.errors = response.data.errors;
          }
        );
    };

    // delete category
    $scope.deleteCategory = function(category)
    {
      $http.delete('/admin/categories/' + category.id)
    		.then(
          function onSuccess(response){
            // remove category from categories
            $scope.categories.splice( $scope.categories.indexOf(category), 1 );
          },
          function onFailure(response){
            $scope.errors = response.data.errors;
          }
        );
    };

  }]);

})(); // end IIFE
