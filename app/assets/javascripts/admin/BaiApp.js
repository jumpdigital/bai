// initialize angular module
var baiApp = angular.module('baiApp', ['ngRoute']);

// configure routes
baiApp.config(function($routeProvider) {
  $routeProvider
    .when('/', {
        templateUrl:  'admin/dashboard',
        controller:   'DashboardController'
    })

    // users
    .when('/users', {
        templateUrl:  'admin/users',
        controller:   'UsersController'
    })
    .when('/users/new', {
        templateUrl:  'admin/users/new',
        controller:   'UsersController'
    })
    .when('/users/:id', {
        templateUrl:  function(params) {
          return 'admin/users/' + params.id;
        },
        controller:   'UsersController'
    })
    .when('/users/:id/edit', {
        templateUrl:  function(params) {
          return 'admin/users/' + params.id + '/edit';
        },
        controller:   'UsersController'
    })

    // customers

    .when('/customers', {
        templateUrl:  'admin/customers',
        controller:   'CustomersController'
    })

    .when('/customers/new', {
        templateUrl:  'admin/customers/new',
        controller:   'CustomersController'
    })
    .when('/customers/:id', {
        templateUrl:  function(params) {
          return 'admin/customers/' + params.id;
        },
        controller:   'CustomersController'
    })
    .when('/customers/:id/edit', {
        templateUrl:  function(params) {
          return 'admin/customers/' + params.id + '/edit';
        },
        controller:   'CustomersController'
    })

    // product categories
    .when('/categories', {
        templateUrl:  'admin/categories',
        controller:   'CategoriesController'
    })
    .when('/categories/new', {
        templateUrl:  'admin/categories/new',
        controller:   'CategoriesController'
    })
    .when('/categories/:id', {
        templateUrl:  function(params) {
          return 'admin/categories/' + params.id;
        },
        controller:   'CategoriesController'
    })
    .when('/categories/:id/edit', {
        templateUrl:  function(params) {
          return 'admin/categories/' + params.id + '/edit';
        },
        controller:   'CategoriesController'
    })

    // products
    .when('/products', {
        templateUrl:  'admin/products',
        controller:   'ProductsController'
    })
    .when('/products/new', {
        templateUrl:  'admin/products/new',
        controller:   'ProductsController'
    })
    .when('/products/:id', {
        templateUrl:  function(params) {
          return 'admin/products/' + params.id;
        },
        controller:   'ProductsController'
    })
    .when('/products/:id/edit', {
        templateUrl:  function(params) {
          return 'admin/products/' + params.id + '/edit';
        },
        controller:   'ProductsController'
    })

    .otherwise({
      redirectTo: "/"
    });
})

.run(function($rootScope) {
  $rootScope.typeOf = function(value) {
    return typeof value;
  };
})

.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value, 10);
      });
    }
  };
});
