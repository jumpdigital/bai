/**
 * UsersController
 */
(function(){

  baiApp.controller('UsersController', [
    '$scope',
    '$http',
    '$location',
    '$routeParams',

    function($scope, $http, $location, $routeParams) {

    $scope.message = 'Welcome to the Users page!';

    $scope.users = null;

    $scope.user = null;

    $scope.roles = null;

    $scope.errors = null;

    // handle redundant $http promises
    var onGetUsersSuccess = function(response){
      $scope.users = response.data;
  	};
  	var onGetUsersFailure = function(response){
  		$scope.errors = response.data.errors;
  	};
    var onGetUserSuccess = function(response){
      $scope.user = response.data;
  	};
  	var onGetUserFailure = function(response){
  		$scope.errors = response.data.errors;
  	};

    // CRUD functions

    // get users
    $scope.getUsers = function()
    {
      $scope.users = $http.get('/admin/users.json')
        .then(onGetUsersSuccess, onGetUsersFailure);
    }

    // show user
    $scope.getUser = function()
    {
      $scope.user = $http.get('/admin/users/' + $routeParams.id + '.json')
        .then(onGetUserSuccess, onGetUserFailure);
    };

    // new user
    $scope.newUser = function()
    {
      $scope.roles = $scope.getRoles();
    };

    // create user
    $scope.createUser = function(user)
    {
      var data = {
        user: user
      }

      $http.post('/admin/users', data)
    		.then(
          function onSucces(response){
            $location.path("/users");
          },
          function onFailure(response){
            $scope.errors = response.data.errors;
            console.log(response.data);
          }
        );
    };

    // edit user
    $scope.editUser = function()
    {
      $scope.user = $scope.getUser();
      $scope.roles = $scope.getRoles();
    };

    // update user
    $scope.updateUser = function(user)
    {
      var data = {
        user: user
      }

      $http.patch('/admin/users/' + user.id, data)
    		.then(
          function onSuccess(response)
          {
            $location.path("/users/" + $scope.user.id);
            $scope.user = response.data.user;
          }
          ,
          function onFailure(response)
          {
            $scope.errors = response.data.errors;
          }
        );
    };

    // delete user
    $scope.deleteUser = function(user)
    {
      $http.delete('/admin/users/' + user.id)
    		.then(
          function onSuccess(response){
            // remove user from users
            $scope.users.splice( $scope.users.indexOf(user), 1 );
          },
          function onFailure(response){
            $scope.errors = response.data.errors;
          }
        );
    };

    // get roles
    $scope.getRoles = function()
    {
      $http.get('/admin/users/roles.json')
    		.then(
          function onSuccess(response){
            $scope.roles = response.data.roles;
          },
          function onFailure(response){
            // restrict creation of new user when no roles
            $location.path("/users");
          }
        );
    };

  }]);

})(); // end IIFE
