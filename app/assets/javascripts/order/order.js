angular.module('bai').factory('orders', ['$http', function($http){
  var o = [];

  var clear = [];

  o.getCart = function() {
  	$http.get('/cart.json')
  	.then(
  		function onSuccess(response){
  			angular.extend(o,response.data);
  			angular.extend(clear, response.data);
  		},
  		function onFailure(response){
  			console.log(response);
  		}
  	);
  };


  
  return o;
}])
