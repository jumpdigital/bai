angular.module('bai')
.controller('NavCtrl', [
'$scope',
'Auth',
function($scope, Auth){
  $scope.signedIn = Auth.isAuthenticated;
  
  $scope.logout = Auth.logout;

  Auth.currentUser().then(function (customer){
    $scope.customer = customer;
  });

  $scope.$on('devise:new-registration', function (e, customer){
    $scope.customer = customer;
  });

  $scope.$on('devise:login', function (e, customer){
    $scope.customer = customer;
  });
  
  $scope.$on('devise:update-user', function (e, customer){
    $scope.customer = customer;
  });

  $scope.$on('devise:logout', function (e, customer){
    $scope.customer = {};
  });    	
}]);