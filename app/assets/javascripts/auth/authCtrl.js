angular.module('bai')
.controller('AuthCtrl', [
'$scope',
'$state',
'Auth',
'$stateParams',
function($scope, $state, Auth,$stateParams){
  console.log($stateParams.reset_password_token);
  $scope.reset_password_token = $stateParams.reset_password_token;
   $scope.customer = { reset_password_token: $stateParams.reset_password_token };

  $scope.isDisabled = false;
  $scope.login = function() {
    $scope.errorMessage = "";
    Auth.login($scope.customer).then(function(){
      $state.go('home');
    }, function errorCallback(response) {
      $scope.errorMessage = response.data.error;
       console.log(response.data);
    });
  };

  $scope.register = function() {
    $scope.errorMessage = "";
    Auth.register($scope.customer).then(function(){
      $state.go('home');
    }, function errorCallback(response) {
      $scope.errorMessage = response.data.errors;
       console.log(response.data.errors);
    });
  };
	 $scope.resetPassword = function() {
    $scope.isDisabled = true;
    $scope.successMessage = "Please wait!";
    $scope.errorMessage = "";
    console.log('')
    Auth.resetPassword($scope.customer).then(function(){
      $state.go('login');
    }, function errorCallback(response) {
      $scope.isDisabled = false;
      $scope.errorMessage = response.data.errors;
       console.log(response.data.errors);
    });
  };

  $scope.forgotPassword = function() {
    $scope.isDisabled = true;
    $scope.successMessage = "Please wait!";
    Auth.sendResetPasswordInstructions($scope.customer).then(function(){
      $scope.isDisabled = false;
      $scope.customer = null;
      $scope.successMessage = "Check your email address.";
    }, function errorCallback(response) {
       $scope.isDisabled = false;
      $scope.errorMessage = response.data.errors;
       console.log(response.data.errors);
    });
  };


  
}]);