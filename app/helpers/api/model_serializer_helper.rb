module Api::ModelSerializerHelper

  def customer_model(obj)
    customer = Hash.new
    customer["id"] = obj.id
    customer["email"] = obj.email
    customer["first_name"] = obj.first_name
    customer["middle_name"] = obj.middle_name
    customer["last_name"] = obj.last_name
    customer["mobile_number"] = obj.mobile_number
    customer["birthdate"] = obj.birthdate
    customer["deactivate"] = obj.deactivate
    customer["note"] = obj.note
    customer["token"] = obj.token
    customer["created_at"] = obj.created_at
    customer["updated_at"] = obj.updated_at
    customer["customer_shipping_address"] = obj.customer_shipping_address
    customer["customer_billing_address"] = obj.customer_billing_address

    return customer

  end

  def order_last_five(obj)
    order = Hash.new
    order["id"] = obj.id
    if obj.customer_id
      order["full_name"] = obj.customer.first_name + " " + obj.customer.last_name
    else
      order["full_name"] = "Valued Customer"
    end

    order["grand_total"] = obj.grand_total
    order["items"] = obj.order_product.sum(:quantity)

    return order


  end


end
