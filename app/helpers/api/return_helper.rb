module Api::ReturnHelper

	def authenticate_token(model)
		@user = request.headers["Authorization"]
		if @user
			@user = model.find_by_token(request.headers["Authorization"])
			if !@user
				unauthorized("Invalid token")
				render :json => @http_response, :status => @http_status
			else
				@user
			end
		else
			not_found("Token not found")
			render :json => @http_response, :status => @http_status
		end


	end

	def success(model)
		@http_status = 200
		@http_response[:status] = true
		@http_response[:results] = model
	end

	def validation_error(model)
		@http_status = 422
		msg = model.errors.full_messages
	    key = model.errors.keys
	    errors ||= Array.new
	    msg.each_with_index do |m, k|
	      full_error = Hash.new(m)
	      full_error = {key[k] => m}
	      errors.push(full_error)
		end
		@http_response[:errors] = errors
	end

	def unauthorized(msg)
		@http_status = 401
		@http_response[:errors] = { :message => msg }
	end

	def not_found(msg)
		@http_status = 422
		@http_response[:errors] = { :message => msg }
	end

	def conflict(msg)
		@http_status = 422
		@http_response[:errors] = { :message => msg }
	end

	def server_error(mdg)
		@http_status = 500
		@http_response[:errors] = { :message => msg }
	end

	def success_message(msg)
		@http_status = 200
		@http_response[:status] = true
		@http_response[:results] = { :message => msg }
	end

	def promo_error(msg)
		@http_status = 422
		@http_response[:errors] = [:message => msg ]
	end

	def order_error(msg)
		@http_status = 422
		@http_response[:errors] = [:message => msg ]
	end

	def order_model(attributes,msg)
		@http_status = 422
		@http_response[:errors] = [attributes => msg ]
	end
end
