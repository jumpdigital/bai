class OrderMailer < ApplicationMailer
  default from: 'notifications@example.com'

  def order_summary(order, customer, setting)
    @customer = customer
    @order = order
    @setting = setting
    if @order.parent_order
      @order_id = @order.parent_order
    else
      @order_id = @order.id
    end
    mail(to: customer["email"], subject: 'Order Summary')

  end

  def order_processing(order, last_history, setting)
    @setting = setting
    @order = order
    if @order.parent_order
      @order_id = @order.parent_order
    else
      @order_id = @order.id
    end
    if @order.customer_id
      @full_name = @order.customer.first_name + " " + @order.customer.last_name
    else
      @full_name = "Valued Customer"
    end
    @last_history = last_history
    mail(to: @order.email, subject: 'Order Processing')
  end

  def order_completed(order, last_history, setting, customer)
    @setting = setting
    @order = order
    if @order.parent_order
      @order_id = @order.parent_order
    else
      @order_id = @order.id
    end
    @last_history = last_history
    if @order.parent_order
      @order_id = @order.parent_order
    else
      @order_id = @order.id
    end
    if @order.customer_id
      @full_name = customer.first_name + " " + customer.last_name
    else
      @full_name = "Valued Customer"
    end

    mail(to: @order.email, subject: 'Order Completed')
  end

  def order_cancel(order, setting)
    @setting = setting
    @order = order
    if @order.parent_order
      @order_id = @order.parent_order
    else
      @order_id = @order.id
    end
    if @order.parent_order
      @order.id = @order.parent_order
    end
    if @order.customer_id
      @full_name = @order.customer.first_name + " " + @order.customer.last_name
    else
      @full_name = "Valued Customer"
    end
    mail(to: @order.email, subject: 'Cancel Order')

  end


end