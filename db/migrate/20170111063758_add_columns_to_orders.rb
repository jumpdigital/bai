class AddColumnsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :totalPromo, :decimal, :precision => 8, :scale => 2
    add_column :orders, :shipping_cost, :decimal, :precision => 8, :scale => 2
    add_column :orders, :vat, :decimal, :precision => 8, :scale => 2
    add_column :orders, :email, :string
  end
end
