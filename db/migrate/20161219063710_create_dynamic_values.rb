class CreateDynamicValues < ActiveRecord::Migration
  def change
    create_table :dynamic_values do |t|
      t.string :model
      t.string :key
      t.string :value
      t.string :description

      t.timestamps null: false
    end
  end
end
