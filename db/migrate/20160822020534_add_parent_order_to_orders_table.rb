class AddParentOrderToOrdersTable < ActiveRecord::Migration
  def change
    add_column :orders, :parent_order, :string
  end
end
