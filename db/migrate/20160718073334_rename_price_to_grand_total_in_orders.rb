class RenamePriceToGrandTotalInOrders < ActiveRecord::Migration
  def change
    rename_column :orders, :price, :grand_total
  end
end
