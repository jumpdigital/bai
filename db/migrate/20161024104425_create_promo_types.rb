class CreatePromoTypes < ActiveRecord::Migration
  def change
    create_table :promo_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
