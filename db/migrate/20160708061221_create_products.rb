class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :sku
      t.string :name
      t.string :description
      t.decimal :price, :precision => 8, :scale => 2
      t.integer :category_id
      t.string :tags

      t.timestamps null: false
    end
  end
end
