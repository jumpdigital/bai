class CreateTaxSettings < ActiveRecord::Migration
  def change
    create_table :tax_settings do |t|
    	t.integer :shipping_zone_country_id
    	t.float :rate 
      	t.timestamps null: false
    end
  end
end
