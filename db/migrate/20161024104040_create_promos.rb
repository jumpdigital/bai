class CreatePromos < ActiveRecord::Migration
  def change
    create_table :promos do |t|
      t.string :name
      t.string :code
      t.string :description
      t.integer :amount_type_id
      t.integer :value
      t.integer :uses_per_coupon
      t.integer :uses_per_customer
      t.datetime :start_at
      t.datetime :expired_at
      t.boolean :status
      t.integer :promo_type_id

      t.timestamps null: false
    end
  end
end
