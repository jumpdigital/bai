class ChangeColumnShipANdVatOrder < ActiveRecord::Migration
  def self.up
  rename_column :orders, :shipping_cost, :shipping_zone_based_rate_id
  change_column :orders, :shipping_zone_based_rate_id, :integer, null: true
  rename_column :orders, :vat, :tax_id
  change_column :orders, :tax_id, :integer, null: true
  end

  def self.down
    change_column :orders, :tax_id, :decimal
    rename_column :orders, :tax_id, :vat
    change_column :orders, :shipping_zone_based_rate_id, :decimal
    rename_column :orders, :shipping_zone_based_rate_id, :shipping_cost
  end

end
