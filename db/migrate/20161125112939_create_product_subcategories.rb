class CreateProductSubcategories < ActiveRecord::Migration
  def change
    create_table :product_subcategories do |t|
      t.integer :product_id
      t.integer :sub_category_id

      t.timestamps null: false
    end
  end
end
