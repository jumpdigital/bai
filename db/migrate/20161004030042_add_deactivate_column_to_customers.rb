class AddDeactivateColumnToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :deactivate, :boolean
  end
end
