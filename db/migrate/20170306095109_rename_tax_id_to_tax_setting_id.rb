class RenameTaxIdToTaxSettingId < ActiveRecord::Migration
  def self.up
  	rename_column :orders, :tax_id, :tax_setting_id
  end

  def self.down
    rename_column :orders, :tax_setting_id, :tax_id
  end
end
