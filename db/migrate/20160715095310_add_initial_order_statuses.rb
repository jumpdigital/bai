class AddInitialOrderStatuses < ActiveRecord::Migration
  def up
    OrderStatus.create(name: "cart")
    OrderStatus.create(name: "pending")
    OrderStatus.create(name: "processing")
    OrderStatus.create(name: "complete")
  end

  def down
    OrderStatus.delete_all
  end
end
