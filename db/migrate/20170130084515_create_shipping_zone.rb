class CreateShippingZone < ActiveRecord::Migration
  def change
    create_table :shipping_zones do |t|
    	
    	t.string :name
    	t.integer :shipping_setting_id
	    t.timestamps null: false
    end
  end
end
