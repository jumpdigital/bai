class CreatePaymentMethodTypes < ActiveRecord::Migration
  def change
    create_table :payment_method_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
