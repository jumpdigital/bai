class CreateShippingZoneCountry < ActiveRecord::Migration
  def change
    create_table :shipping_zone_countries do |t|
    	t.string :country
    	t.integer :shipping_zone_id
	    t.timestamps null: false
    end
  end
end
