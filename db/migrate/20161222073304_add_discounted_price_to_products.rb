class AddDiscountedPriceToProducts < ActiveRecord::Migration
  def change
    add_column :products, :discounted_price, :decimal, :precision => 8, :scale => 2
    add_column :products, :discount_date_start, :datetime
    add_column :products, :discount_date_end, :datetime
  end
end
