class AddFieldsToOrdersTable < ActiveRecord::Migration
  def change
  	add_column :orders, :billing_address, :string
  	add_column :orders, :billing_country, :string
  	add_column :orders, :billing_city, :string
  	add_column :orders, :billing_zip_code, :string
  	add_column :orders, :shipping_address, :string
  	add_column :orders, :shipping_country, :string
  	add_column :orders, :shipping_city, :string
  	add_column :orders, :shipping_zip_code, :string

  end
end
