class CreatePaymentTransactions < ActiveRecord::Migration
  def change
    create_table :payment_transactions do |t|
      t.integer :order_id
      t.integer :payment_method_id
      t.decimal :amount, :precision => 8, :scale => 2

      t.timestamps null: false
    end
  end
end
