class CreateGeneralSetting < ActiveRecord::Migration
  def change
    create_table :general_settings do |t|
      t.integer :user_id
      t.string :store_name
      t.string :account_email
      t.string :store_logo, null:true
      t.string :legal_name_of_business, null:true
      t.string :phone, null:true
      t.string :street, null:true
      t.string :apt_suite, null: true
      t.string :timezone
      t.string :unit_system
      t.string :weight_unit
      t.string :currency
      t.timestamps null: false
    end
  end
end
