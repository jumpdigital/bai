class CreatePaymentMethods < ActiveRecord::Migration
  def change
    create_table :payment_methods do |t|
      t.string :name
      t.integer :payment_method_type_id
      t.string :key
      t.string :value

      t.timestamps null: false
    end
  end
end
