class AddFieldsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :weight, :string
    add_column :products, :unit, :string
  end
end
