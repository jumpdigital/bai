class AddDeactivateToUser < ActiveRecord::Migration
  def change
    add_column :users, :deactivate, :boolean
  end
end
