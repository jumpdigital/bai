class CreatePaypalTransactions < ActiveRecord::Migration
  def change
    create_table :paypal_transactions do |t|
      t.integer :payment_transaction_id
      t.string :paypal_account_id
      t.string :capture_payment_id

      t.timestamps null: false
    end
  end
end
