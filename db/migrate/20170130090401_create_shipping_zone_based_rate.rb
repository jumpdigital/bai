class CreateShippingZoneBasedRate < ActiveRecord::Migration
  def change
    create_table :shipping_zone_based_rates do |t|
    	t.integer :shipping_zone_id
    	t.integer :based_rate_type
        t.string :name
    	t.decimal :minimum, :precision=> 64, :scale=> 12
    	t.decimal :maximum, :precision=> 64, :scale=> 12
    	t.boolean :is_free, :default => false
        t.decimal :amount, :precision=> 64, :scale=> 12
    	t.timestamps null: false
    end
  end
end
