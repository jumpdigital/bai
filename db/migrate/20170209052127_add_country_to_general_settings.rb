class AddCountryToGeneralSettings < ActiveRecord::Migration
  def change
  	add_column :general_settings, :city, :string
    add_column :general_settings, :zip_code, :string
    add_column :general_settings, :country, :string
  end
end
