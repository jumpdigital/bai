class CreateOrderProducts < ActiveRecord::Migration
  def change
    create_table :order_products do |t|
      t.integer :order_id
      t.integer :product_id
      t.integer :quantity
      t.decimal :total_price, :precision => 8, :scale => 2

      t.timestamps null: false
    end
  end
end
