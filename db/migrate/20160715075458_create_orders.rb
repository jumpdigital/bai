class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|

      t.integer :customer_id

      t.integer :payment_transaction_id
      t.decimal :price, :precision => 8, :scale => 2
      t.integer :promo_id

      t.timestamps null: false
    end
  end
end
