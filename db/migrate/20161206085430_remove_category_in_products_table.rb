class RemoveCategoryInProductsTable < ActiveRecord::Migration
  def change
    remove_column :products, :category_id
    remove_column :products, :sub_category_id
  end
end
