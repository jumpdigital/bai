class CreateCreditCardTransactions < ActiveRecord::Migration
  def change
    create_table :credit_card_transactions do |t|
      t.integer :payment_transaction_id

      t.timestamps null: false
    end
  end
end
