class CreateCustomerShippingAddresses < ActiveRecord::Migration
  def change
    create_table :customer_shipping_addresses do |t|
      t.integer :customer_id
      t.string :street_address
      t.string :country
      t.string :city
      t.string :zip_code
      t.boolean :default

      t.timestamps null: false
    end
  end
end
