class CreateProductTags < ActiveRecord::Migration
  def change
    create_table :product_tags do |t|
      t.integer :product_id
      t.string :tag

      t.timestamps null: false
    end
  end
end
