class CreatePromoSelecteds < ActiveRecord::Migration
  def change
    create_table :promo_selecteds do |t|
      t.integer :promo_id
      t.integer :selected_id

      t.timestamps null: false
    end
  end
end
