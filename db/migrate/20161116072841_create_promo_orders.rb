class CreatePromoOrders < ActiveRecord::Migration
  def change
    create_table :promo_orders do |t|
      t.integer :order_id
      t.integer :promo_id

      t.timestamps null: false
    end
  end
end
