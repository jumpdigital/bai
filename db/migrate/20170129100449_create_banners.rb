class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :name
      t.string :normal_photo
      t.string :retina_photo

      t.timestamps null: false
    end
  end
end
