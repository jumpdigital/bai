class CreateShippingSetting < ActiveRecord::Migration
  def change
    create_table :shipping_settings do |t|
    	t.integer :general_setting_id
    	t.string :street
    	t.string :apt_suite
    	t.string :city
    	t.string :zip_code
    	t.string :country
    	t.string :region
    	t.string :phone
	    t.timestamps null: false
    end
  end
end
