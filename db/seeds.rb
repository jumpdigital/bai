# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# seed roles table
Role.create(
		name: "Super Admin",
		description: "Should be able to manage everything."
	)

Role.create(
		name: "Admin",
		description: "Should be able to manage general things."
	)

Product.create(
		sku: "SKU",
		name: "Macbook Pro",
		description: "Macbook pro is a laptop",
		price: 100000.2,
		category_id: 1,
		tags: "Laptop"
	)
